{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeApplications    #-}

-- | This module contains a whole bunch of orphan instances for Static
-- constraints. Ideally, many of these should be moved to appropriate
-- locations so that they are no longer orphans.

module Hyperion.Static.Orphans where

import Bootstrap.Math.FreeVect (FreeVect)
import           Data.Binary     (Binary)
import           Data.Proxy      (Proxy)
import           GHC.TypeNats    (KnownNat)
import           Hyperion.Static (Dict (..), Static (..), cAp, cPtr)
import           Linear.V        (V)
import           Numeric.Rounded (Rounded, RoundingMode (..))
import           Type.Reflection (Typeable)

instance Static (Integral Integer) where
  closureDict = cPtr (static Dict)

instance (KnownNat n, Typeable a, Static (Binary a)) => Static (Binary (V n a)) where
  closureDict = cPtr (static (\Dict Dict -> Dict)) `cAp`
                closureDict @(Binary a) `cAp`
                closureDict @(KnownNat n)

instance KnownNat p => Static (Binary (Rounded 'TowardZero p)) where
  closureDict = cPtr (static (\Dict -> Dict)) `cAp`
                closureDict @(KnownNat p)

instance Binary (Proxy a) where

instance (Typeable k, Typeable (a :: k)) => Static (Binary (Proxy a)) where
  closureDict = cPtr (static Dict)

instance (Typeable a, Static (Binary a), Typeable b, Static (Binary b)) => Static (Binary (FreeVect b a)) where
  closureDict = cPtr (static (\Dict Dict -> Dict)) `cAp`
                closureDict @(Binary a) `cAp`
                closureDict @(Binary b)
