{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}

-- | This module implements a higher-dimensional generalization of a
-- binary search. To define such a search, we would like to first
-- build a probability function that maps each point to its likelihood
-- to be either True or False. The central idea of DelaunaySearch is
-- to build such a function using a Delaunay triangulation between
-- known points and linear interpolation. We assign True vertices
-- probability 1 and False vertices probability 0, while the
-- probability within a Delaunay simplex is determined by linear
-- interpolation. (We exclude "incomplete" simplices that possess
-- vertices that haven't finished being computed yet.)
--
-- Given the probability funciton, we can define a 50% surface
-- consisting of points whose probability of being True or Talse is
-- 50%. We would like to choose one of the points on the 50% surface
-- to test next. Unfortunately, it is not completely clear what the
-- best choice is, so the user can specify a 'ChoiceMethod' described
-- below.
--
-- Note that this definition of a probability function is not
-- guaranteed to work in all circumstances. It requires that the
-- initial set of known points be sufficiently dense compared to the
-- "wiggliness" of the actual boundary between True and
-- False. However, it seems to work well in many examples.

module Hyperion.Bootstrap.DelaunaySearch where

import Bootstrap.Math.AffineTransform      qualified as AT
import Bootstrap.Math.Linear               ()
import Control.Applicative                 ((<|>))
import Control.Distributed.Process         (Process)
import Control.Monad.Base                  (MonadBase)
import Control.Monad.Catch                 (MonadCatch)
import Control.Monad.IO.Class              (MonadIO)
import Control.Monad.Reader                (MonadReader)
import Control.Monad.Trans.Maybe           (MaybeT (..), runMaybeT)
import Data.Foldable                       (maximumBy, toList)
import Data.List.NonEmpty                  (NonEmpty, nonEmpty)
import Data.Map                            (Map)
import Data.Map                            qualified as Map
import Data.Maybe                          (mapMaybe)
import Data.Ord                            (comparing)
import Data.Semigroup                      (sconcat)
import Data.Time.Clock                     (UTCTime)
import Data.Vector                         qualified as V
import GHC.TypeNats                        (KnownNat)
import Hyperion.Bootstrap.PointCloudSearch qualified as PC
import Hyperion.Bootstrap.Qdelaunay        (qdelaunay)
import Hyperion.Concurrent                 (Concurrently)
import Hyperion.Database                   qualified as DB
import Hyperion.ExtVar                     (newExtVarStream)
import Hyperion.Log                        qualified as Log
import Linear.Metric                       (distance)
import Linear.V                            (Dim, V)
import Linear.V                            qualified as L
import Linear.Vector                       (sumV, (^/))

data DelaunayConfig = DelaunayConfig
  { qdelaunayExecutable :: FilePath
  , choiceMethod        :: ChoiceMethod
  , nThreads            :: Int
  , nSteps              :: Int
  , terminateTime       :: Maybe UTCTime
  } deriving (Eq, Ord, Show)

-- | Some terminology:
--
-- "mixed edge": an edge of a simplex such that one endpoint is True
-- and the other endpoint is False.
--
-- "candidate point": the average of the midpoints of the mixed edges
-- (note this is only used in 'ChangeDistance' and
-- 'CandidateSeparation').
--
data ChoiceMethod
  -- | Given a Delaunay triangulation, find the simplex with the
  -- largest minimum length of its mixed edges, and choose its
  -- candidate point.
  = ChangeDistance
  -- | Given a Delaunay triangulation, find the simplex whose
  -- candidate point is furthest from its vertices (i.e. largest
  -- minimum distance to a vertex), and choose its candidate point.
  | CandidateSeparation
  -- | Given a Delaunay triangulation, choose the midpoint of the
  -- longest mixed edge.  Another way to think about this choice is
  -- that such a point is always on the intersection of a face of the
  -- Voronoi mesh with the 50% surface (defined above).
  | LongestMixedEdgeMidpoint
  deriving (Eq, Ord, Show)

-- | The maximum of a NonEmpty list, measured by 'g'.
maximumOn :: Ord b => (a -> b) -> NonEmpty a -> a
maximumOn g = maximumBy (comparing g)

-- | A list of points, with data 'a' assigned to each point.
type Simplex n a = [ (V n Rational, a) ]

-- | A Simplex where some of the vertices are not computed yet. Maybe
-- Bool is used to encode the status of the computation involving each
-- point. Nothing indicates the point is still being computed. Just
-- (True|False) indicates the computation has completed.
type IncompleteSimplex n = Simplex n (Maybe Bool)

-- | A Simplex where all the vertices have been computed.
type CompleteSimplex n = Simplex n Bool

-- | A "smart constructor" for a CompleteSimplex.
completeSimplex :: Simplex n (Maybe Bool) -> Maybe (CompleteSimplex n)
completeSimplex = traverse tupM
  where
    tupM (a, mb) = fmap (\b -> (a,b)) mb

-- | A pair of points
type Segment n = (V n Rational, V n Rational)

-- | The length of a Segment, approximated as a 'Double'
segmentLength :: Dim n => Segment n -> Double
segmentLength (p,q) = distance (fmap fromRational p) (fmap fromRational q)

-- | The midpoint of a Segment
segmentMidpoint :: Dim n => Segment n -> V n Rational
segmentMidpoint (p, q) = (p + q) ^/ 2

-- | A Simplex whose vertices are "mixed" -- i.e. neither all True nor
-- all False, together with a NonEmpty list of edges that cross
-- between 'True' and 'False'. The NonEmpty list is evidence that the
-- values of the vertices are not all the same.
data MixedSimplex n = MkMixedSimplex
  { simplex    :: CompleteSimplex n
  , mixedEdges :: NonEmpty (Segment n)
  }

-- | Edges that connect vertices with different boolean values,
-- oriented from True to False.
simplexMixedEdges :: CompleteSimplex n -> [Segment n]
simplexMixedEdges simplex = do
  (p, True)  <- simplex
  (q, False) <- simplex
  pure (p,q)

-- | A "smart constructor" for a MixedSimplex
mixedSimplex :: CompleteSimplex n -> Maybe (MixedSimplex n)
mixedSimplex simplex = do
  mEdges <- nonEmpty (simplexMixedEdges simplex)
  pure (MkMixedSimplex simplex mEdges)

-- | Minimum length of the mixed edges of a simplex.
simplexChangeDistance :: Dim n => MixedSimplex n -> Double
simplexChangeDistance simplex =
  minimum (fmap segmentLength simplex.mixedEdges)

-- | The average of the midpoints of the mixed edges for the simplex
-- with the largest value of 'simplexChangeDistance'
largestChangeDistanceMidpointAverage :: Dim n => NonEmpty (MixedSimplex n) -> V n Rational
largestChangeDistanceMidpointAverage =
  mixedEdgeMidpointAverage . maximumOn simplexChangeDistance

-- | Minimum distance between candidate point and other vertices. If
-- all vertices have the same boolean values, simplexCandidateSeparation is
-- zero.
simplexCandidateSeparation :: Dim n => MixedSimplex n -> Double
simplexCandidateSeparation simplex =
  let c = mixedEdgeMidpointAverage simplex
  in minimum [segmentLength (p,c) | (p,_) <- simplex.simplex]

-- | Average of the midpoints of the mixed edges for the simplex with
-- the largest value of 'simplexCandidateSeparation'.
largestCandidateSeparationMidpointAverage :: Dim n => NonEmpty (MixedSimplex n) -> V n Rational
largestCandidateSeparationMidpointAverage =
  mixedEdgeMidpointAverage . maximumOn simplexCandidateSeparation

-- | Average of the midpoints of the mixedEdges
mixedEdgeMidpointAverage :: Dim n => MixedSimplex n -> V n Rational
mixedEdgeMidpointAverage simplex =
  sumV [ p + q | (p, q) <- toList simplex.mixedEdges ]
  ^/ fromIntegral (2 * length simplex.mixedEdges)

-- | The midpoint of the longest "mixed edge" in the given list of
-- MixedSimplices
longestMixedEdgeMidpoint :: Dim n => NonEmpty (MixedSimplex n) -> V n Rational
longestMixedEdgeMidpoint =
  segmentMidpoint . maximumOn segmentLength . sconcat . fmap (.mixedEdges)

-- | Use the given 'ChoiceMethod' to select the next test point, given
-- a NonEmpty list of MixedSimplices.
choosePoint :: Dim n => ChoiceMethod -> NonEmpty (MixedSimplex n) -> V n Rational
choosePoint method = case method of
  ChangeDistance           -> largestChangeDistanceMidpointAverage
  CandidateSeparation      -> largestCandidateSeparationMidpointAverage
  LongestMixedEdgeMidpoint -> longestMixedEdgeMidpoint

-- | Run qdelaunay to compute a Delaunay triangulation and return the
-- resulting list of simplices, labeled by the values in the given
-- pointMap.
getDelaunaySimplices :: DelaunayConfig -> Map (V n Rational) a -> IO [Simplex n a]
getDelaunaySimplices config pointMap =
  map toSimplex <$>
  qdelaunay config.qdelaunayExecutable
  (fmap (L.toVector . fmap fromRational) points)
  where
    points = V.fromList (Map.keys pointMap)
    toSimplex vertexIndices = do
      i <- V.toList vertexIndices
      let p = points V.! i
      return (p, pointMap Map.! p)

-- | Given a point map (assignments of points to 'Maybe Bool'),
-- compute a Delaunay triangulation and use config.choiceMethod to
-- select the next point to search.
delaunaySearchPoint
  :: Dim n
  => DelaunayConfig
  -> Map (V n Rational) (Maybe Bool)
  -> IO (Maybe (V n Rational))
delaunaySearchPoint config pointMap = do
  incompleteSimplices <- getDelaunaySimplices config pointMap
  let
    completeSimplices = mapMaybe completeSimplex incompleteSimplices
    mixedSimplices    = mapMaybe mixedSimplex completeSimplices
  Log.info "Delaunay search: total simplices" (length incompleteSimplices)
  Log.info "Delaunay search: searchable simplices" (length mixedSimplices)
  pure $ choosePoint config.choiceMethod <$> nonEmpty mixedSimplices

-- | Perform a Delaunay search over a region and persist the results
-- to a database.
--
-- In addition, we define an 'ExtVar' for a list of points [V n
-- Rational]. The 'ExtVar' is consulted *first* when determining the
-- next point to use. Thus, by inserting points into the 'ExtVar', the
-- user can choose the next points for the Delaunay search by hand
-- (pre-empting the use of Delaunay triangulation). Note that the
-- points in the 'ExtVar' are in the frame associated to the
-- 'AffineTransform' -- in other words, they are in the frame where
-- the allowed region should be roughly a sphere with O(1) radius.
--
-- To use the 'ExtVar', look for a line in the logs of the form
--
-- > [Fri 01/07/22 13:00:32] ExtVar for delaunaySearch: extVar ...
--
-- You can interact with the 'ExtVar' via GHCi -- see the
-- documentation in 'Hyperion.ExtVar' for details.
--
delaunaySearchRegionPersistent
  :: forall n m env .
     ( KnownNat n, MonadIO m, MonadReader env m, DB.HasDB env, MonadCatch m
     , Applicative (Concurrently m), MonadBase Process m
     )
  => DB.KeyValMap (V n Rational) (Maybe Bool)
  -> DelaunayConfig
  -> AT.AffineTransform n Rational
  -> Map (V n Rational) (Maybe Bool)
  -> (V n Rational -> m Bool)
  -> m PC.PointCloudStatus
delaunaySearchRegionPersistent results config affine initialPts f = do
  (eVar, getExtStream) <- newExtVarStream []
  Log.info "ExtVar for delaunaySearch" eVar
  let
    pointCloudCfg = PC.PointCloudConfig
      { PC.nThreads        = config.nThreads
      , PC.nSteps          = const config.nSteps
      , PC.getNextPoint    = \pointMap -> runMaybeT $
                                          MaybeT getExtStream <|>
                                          MaybeT (delaunaySearchPoint config pointMap)
      , PC.addBoundingCube = False
      , PC.terminateTime   = config.terminateTime
      }
  PC.pointCloudSearchRegionPersistent results pointCloudCfg affine initialPts f

