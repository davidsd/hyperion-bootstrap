{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Hyperion.Bootstrap.PointCloudSearch where

import Bootstrap.Math.AffineTransform qualified as AT
import Bootstrap.Math.Linear          ()
import Control.Applicative            ((<|>))
import Control.Concurrent.MVar
import Control.Concurrent.STM         (atomically)
import Control.Concurrent.STM.TChan   qualified as TChan
import Control.Concurrent.TrackedVar
import Control.Monad                  (filterM, void)
import Control.Monad.Catch            (MonadCatch)
import Control.Monad.IO.Class         (MonadIO, liftIO)
import Control.Monad.Reader           (MonadReader)
import Data.Binary                    (Binary)
import Data.Map                       (Map)
import Data.Map                       qualified as Map
import Data.Maybe                     (isNothing)
import Data.Time.Clock                (UTCTime, getCurrentTime)
import Data.Vector                    qualified as V
import GHC.Generics                   (Generic)
import GHC.TypeNats                   (KnownNat)
import Hyperion.Concurrent            (Concurrently, replicateConcurrently)
import Hyperion.Database              qualified as DB
import Hyperion.Log                   qualified as Log
import Linear.V                       (Dim, V)
import Linear.V                       qualified as L
import Linear.Vector                  (basis, sumV, zero, (^*))

type PointMap n = Map (V n Rational) (Maybe Bool)

data PointCloudConfig n = PointCloudConfig
  { nThreads        :: Int
  , nSteps          :: PointMap n -> Int
  , addBoundingCube :: Bool
  , getNextPoint    :: PointMap n -> IO (Maybe (V n Rational))
  , terminateTime   :: Maybe UTCTime
  }

data PointCloudStatus = Finished | Incomplete
  deriving (Eq, Ord, Show, Generic, Binary)

instance Semigroup PointCloudStatus where
  Finished <> Finished = Finished
  _ <> _               = Incomplete

instance Monoid PointCloudStatus where
  mempty = Finished

pointCloudSearchNext
  :: PointCloudConfig n
  -> MVar (TrackedVar (Map (V n Rational) (Maybe Bool)))
  -> IO (V n Rational)
pointCloudSearchNext cfg tMapVar =
  -- take tMapVar and do not release until nextPoint has been
  -- determined and added to the map
  takeMVar tMapVar >>= go
  where
    go tMap = do
      -- read tMap and mark it as old
      pointMap <- readTrackedVar tMap
      getNextPoint cfg pointMap >>= \case
        Just nextPoint -> do
          Log.info "Next point" nextPoint
          -- add nextPoint to the map
          modifySilentTrackedVar tMap (Map.insert nextPoint Nothing)
          readSilentTrackedVar tMap >>= logPointMap
          -- release tMapVar
          putMVar tMapVar tMap
          return nextPoint
        Nothing -> do
          -- wait until tMap has been updated before trying again
          waitTrackedVar tMap
          go tMap

insertMany :: Ord a => [(a, b)] -> Map a b -> Map a b
insertMany kvs = Map.union (Map.fromList kvs)

limitWithCounter :: MonadIO m => Maybe UTCTime -> MVar Int -> m PointCloudStatus -> m PointCloudStatus
limitWithCounter stopTime counter m = do
  c <- liftIO (takeMVar counter)
  now <- liftIO getCurrentTime
  if
    | c > 0 && maybe True (now <) stopTime ->
        liftIO (putMVar counter (c-1)) >> m
    | c > 0 ->
        liftIO (putMVar counter c) >> pure Incomplete
    | otherwise ->
        liftIO (putMVar counter c) >> pure Finished

pointCloudSearch
  :: (MonadIO m, Applicative (Concurrently m))
  => PointCloudConfig n
  -> Map (V n Rational) (Maybe Bool)
  -> (V n Rational -> m Bool)
  -> m PointCloudStatus
pointCloudSearch cfg initialPoints f = do
  tMap <- liftIO $ newTrackedVar initialPoints
  tMapVar <- liftIO $ newMVar tMap
  counter <- liftIO $ newMVar (nSteps cfg initialPoints)
  initialUnknownPtsChan <- liftIO $ TChan.newTChanIO
  liftIO $ atomically $ mapM_ (TChan.writeTChan initialUnknownPtsChan) $
    Map.keys $ Map.filter isNothing initialPoints
  let
    getInitialUnknownPt = liftIO $ atomically (TChan.tryReadTChan initialUnknownPtsChan)
    computePoint = do
      nextPoint <- getInitialUnknownPt >>= \case
        Nothing -> liftIO (pointCloudSearchNext cfg tMapVar)
        Just p  -> return p
      result <- f nextPoint
      liftIO $ modifyTrackedVar tMap (Map.insert nextPoint (Just result))
    go = limitWithCounter (terminateTime cfg) counter $ do
      -- TODO: Do we want to catch errors here in case computePoint fails?
      computePoint
      go
  statuses <- replicateConcurrently (nThreads cfg) go
  pure $ mconcat statuses

logPointMap :: Map (V n Rational) (Maybe Bool) -> IO ()
logPointMap pointMap =
  Log.info "Point cloud search points" pointList
  where
    pointList = do
      val <- [Just True, Just False, Nothing]
      let pts = Map.keys (Map.filter (== val) pointMap)
      return $ map (map (fromRational @Double) . V.toList . L.toVector) pts

powerSet :: [a] -> [[a]]
powerSet = filterM (const [True, False])

unitCube :: (Num a, Dim n) => [V n a]
unitCube = map sumV (powerSet basis)

duoCubeCentered :: (Num a, Dim n) => [V n a]
duoCubeCentered = map (subtract (sumV basis) . (^* 2)) unitCube

boundedByCube
  :: KnownNat n
  => AT.AffineTransform n Rational
  -> Map (V n Rational) (Maybe Bool)
  -> Map (V n Rational) (Maybe Bool)
boundedByCube a pts = Map.union (assumeCenterIfNoTrue pts) boundaryMap
  where
    assumeCenterIfNoTrue m =
      if Map.null (Map.filter (== Just True) m)
      then Map.insert (AT.apply a zero) (Just True) m
      else m
    boundaryMap = Map.fromList [(AT.apply a v, Just False) | v <- duoCubeCentered]

-- | If no True initial points are provided, defaults to (regionOrigin
-- r). False initial points are supplemented with duoCubeCentered if
-- 'addBoundingCube' is True.
pointCloudSearchRegion
  :: (KnownNat n, MonadIO m, Applicative (Concurrently m))
  => PointCloudConfig n
  -> AT.AffineTransform n Rational
  -> Map (V n Rational) (Maybe Bool)
  -> (V n Rational -> m Bool)
  -> m PointCloudStatus
pointCloudSearchRegion cfg a initialPts f =
  pointCloudSearch cfg
    (Map.mapKeys (AT.apply (AT.inverse a))
     (if addBoundingCube cfg then boundedByCube a initialPts else initialPts))
    (f . AT.apply a)

-- | A version of pointCloudSearchRegion that loads initial points from a
-- database and saves each point to the database as it finishes.
--
-- TODO: Store the number of steps taken in the database?
pointCloudSearchRegionPersistent
  :: forall n m env .
     ( KnownNat n, MonadIO m, MonadReader env m, DB.HasDB env, MonadCatch m
     , Applicative (Concurrently m)
     )
  => DB.KeyValMap (V n Rational) (Maybe Bool)
  -> PointCloudConfig n
  -> AT.AffineTransform n Rational
  -> Map (V n Rational) (Maybe Bool)
  -> (V n Rational -> m Bool)
  -> m PointCloudStatus
pointCloudSearchRegionPersistent results cfg affine initialPts f = do
  storedPts <- Map.fromList <$> DB.lookupAll results
  -- Combine initialPts and storedPts, preferring known results
  -- (i.e. Just True/False instead of Nothing)
  let startPts = Map.unionWith (<|>) storedPts initialPts
  -- Insert all of startPts into the database. storedPts will already
  -- be in the database (they will be duplicated), but initialPts may
  -- not be. In this way, the database becomes the single source of
  -- truth for the result of the search.
  void $ Map.traverseWithKey (DB.insert results) startPts
  pointCloudSearchRegion cfg affine startPts f'
  where
    f' v = do
      DB.insert results v Nothing
      result <- f v
      DB.insert results v (Just result)
      return result
