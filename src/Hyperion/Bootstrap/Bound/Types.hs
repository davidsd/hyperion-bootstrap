{-# LANGUAGE DataKinds               #-}
{-# LANGUAGE DefaultSignatures       #-}
{-# LANGUAGE DeriveAnyClass          #-}
{-# LANGUAGE DuplicateRecordFields   #-}
{-# LANGUAGE ImpredicativeTypes      #-}
{-# LANGUAGE QuantifiedConstraints   #-}
{-# LANGUAGE StaticPointers          #-}
{-# LANGUAGE TypeFamilies            #-}
{-# LANGUAGE TypeOperators           #-}
{-# LANGUAGE UndecidableInstances    #-}
{-# LANGUAGE UndecidableSuperClasses #-}

module Hyperion.Bootstrap.Bound.Types
  ( Bound(..)
  , Precision(..)
  , BigFloat
  , reifyPrecision
  , reflectPrecision
  , BoundDirection(..)
  , boundDirSign
  , BoundConfig(..)
  , BoundFiles(..)
  , FileTreatment(..)
  , BoundFileTreatment(..)
  , SDPFetchValue
  , BoundKeyVals
  , ToSDP(..)
  , SDPFetchBuildConfig(..)
  ) where

import Bootstrap.Build            (All, ComputeDependencies, DropVoid,
                                   FetchConfig, FetchesAll, GetDependencies,
                                   HasForce, Keys, MemoFetchT, SomeBuildChain,
                                   Sum, SumDropVoid)
import Bootstrap.Math.Linear.Util ()
import Control.Monad.IO.Class     (MonadIO)
import Data.Aeson                 (FromJSON, ToJSON(..))
import Data.Binary                (Binary)
import Data.Data                  (Typeable)
import Data.Kind                  (Constraint, Type)
import Data.Proxy                 (Proxy)
import Data.Reflection            (reifyNat)
import GHC.Generics               (Generic)
import GHC.TypeNats               (KnownNat, Nat, natVal)
import Hyperion.Job               (Job)
import Hyperion.Static            (Dict (..), Static (..), cAp)
import SDPB                       (BigFloat, SDP)
import SDPB qualified

-- | Data needed to compute a bound.
data Bound prec b = Bound
  { -- | The type of bound
    boundKey     :: b
    -- | Precision at which the PMP will be computed. We leave this as
    -- a parameter because precision will sometimes be a concrete
    -- 'Int', and will sometimes be stored at the type level.
  , precision    :: prec
    -- | SDPB parameters to use when computing the bound
  , solverParams :: SDPB.Params
    -- | Configuration data, e.g. directory with executable scripts.
  , boundConfig  :: BoundConfig
  } deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON, Functor)

instance (Typeable b, Typeable prec, Static (Binary b), Static (Binary prec)) =>
  Static (Binary (Bound prec b)) where
  closureDict = static (\Dict Dict -> Dict) `cAp` closureDict @(Binary b) `cAp` closureDict @(Binary prec)

-- | A type level precision value
data Precision p = MkPrecision
  deriving (Eq, Ord, Generic, Binary)

-- | Reify an Int to a 'Precision', storing the Int at the type level.
reifyPrecision :: Int -> (forall (p :: Nat) . KnownNat p => Precision p -> r) -> r
reifyPrecision n go = reifyNat (toInteger n) $ \(_ :: Proxy p) -> go (MkPrecision @p)

-- | Convert a 'Precision' back to an 'Int'
reflectPrecision :: KnownNat p => Precision p -> Int
reflectPrecision = fromIntegral . natVal

instance KnownNat p => Show (Precision p) where
  show p = "MkPrecision @" <> show (natVal p)

instance (Typeable k, Typeable (p :: k)) => Static (Binary (Precision p)) where
  closureDict = static Dict

-- | Note that we cannot easily define a FromJSON instance because of
-- the type-level variable. Generally, you should reflectPrecision
-- first if you want two-way tripping with JSON.
instance KnownNat p => ToJSON (Precision p) where
  toJSON p = toJSON $ natVal p

-- | Two different kinds of bounds. NB: This should probably live in
-- Bootstrap.Bound somewhere.
data BoundDirection
  = UpperBound
  | LowerBound
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

boundDirSign :: Num a => BoundDirection -> a
boundDirSign UpperBound = 1
boundDirSign LowerBound = -1

-- | Configuration data for a Bound.
data BoundConfig = BoundConfig { scriptsDir :: FilePath }
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON)

instance Static (Binary BoundConfig) where closureDict = static Dict

data BoundFiles = BoundFiles
  { jsonDir              :: FilePath
  , blockDir             :: FilePath
  , sdpDir               :: FilePath
  , outDir               :: FilePath
  , checkpointDir        :: FilePath
  , initialCheckpointDir :: Maybe FilePath
  } deriving (Show, Generic, Binary, ToJSON, FromJSON)

instance Static (Binary BoundFiles) where closureDict = static Dict

data FileTreatment = KeepFile | RemoveFile
  deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

data BoundFileTreatment = MkBoundFileTreatment
  { jsonDirTreatment       :: FileTreatment
  , blockDirTreatment      :: FileTreatment
  , sdpDirTreatment        :: FileTreatment
  , outDirTreatment        :: FileTreatment
  , checkpointDirTreatment :: FileTreatment
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

instance Static (Binary BoundFileTreatment) where closureDict = static Dict

-- | A type family mapping a key 'k' to the corresponding value, where
-- the value can depend on a numerical type 'a'. Any library that
-- defines something that can be fetched as part of building a Bound
-- (for example scalar-blocks, blocks-3d, etc) should define an
-- instance of SDPFetchValue for the corresponding key type.
type family SDPFetchValue a k :: Type

type family ToKeyVals a ks :: [(Type, Type)] where
  ToKeyVals a '[] = '[]
  ToKeyVals a (k ': ks) = '(k, SDPFetchValue a k) ': (ToKeyVals a ks)

-- | Types of (key, value) pairs to be fetched when computing a Bound.
type BoundKeyVals b p = ToKeyVals (BigFloat p) (SDPFetchKeys b)

-- | A constraint indicating that the key-value pairs for a given
-- bound type 'b' can be fetched in the helper Applicatives
-- 'MemoFetchT', 'ComputeDependencies', 'GetDependencies', etc. This
-- constraint should be trivially satisfied for any concrete
-- collection of key-value pairs. But GHC must prove that separately
-- in each concrete case.
type CanFetchBound b =
  ( forall p m . (KnownNat p, Monad m) => FetchesAllMemoFetchT b p m
  , forall p . KnownNat p => CanFetchKeyVals b p
  ) :: Constraint

-- | A constraint indicating that the 'BoundKeyVals b p' can be
-- fetched in 'ComputeDependencies' and 'GetDependencies'.
--
-- [Note: class newtypes] There is a current limitation of
-- QuantifiedConstraints that the right-hand side cannot be a tuple
-- constraint or type family. Since we want to use 'CanFetchKeyValsC'
-- on the right-hand side of a QuantifiedConstraint in
-- 'CanFetchBound', we must define a "class newtype", see
-- https://www.youtube.com/live/UkkKyobdzTU?t=1087&feature=shared.
--
-- The idea of this trick is suppose we have a general constraint
-- 'FooC a b c', which can be a tuple or the result of a TypeFamily or
-- both, and then define an equivalent class 'Foo' as follows:
--
-- > type FooC a b c = ...
--
-- > class    FooC a b c => Foo a b c
-- > instance FooC a b c => Foo a b c
--
-- For all practical purposes, 'Foo a b c' is equivalent to 'FooC a b
-- c'. However, 'Foo a b c' is now a non-tuple/TypeFamily constraint
-- and can appear on the RHS of a QuantifiedConstraint.  We say that
-- 'Foo' is a "class newtype" for 'FooC'.
type CanFetchKeyValsC b p =
  ( FetchesAll (BoundKeyVals b p) (ComputeDependencies (BoundKeyVals b p))
  , FetchesAll (BoundKeyVals b p) (GetDependencies (BoundKeyVals b p))
  , DropVoid (Sum (Keys (BoundKeyVals b p))) (SumDropVoid (SDPFetchKeys b))
  , All Ord (Keys (BoundKeyVals b p))
  ) :: Constraint

-- | A "class newtype" for 'CanFetchKeyValsC'.
class    CanFetchKeyValsC b p => CanFetchKeyVals b p
instance CanFetchKeyValsC b p => CanFetchKeyVals b p

-- | A "class newtype" for the LHS
class    FetchesAll (BoundKeyVals b p) (MemoFetchT (BoundKeyVals b p) m) => FetchesAllMemoFetchT b p m
instance FetchesAll (BoundKeyVals b p) (MemoFetchT (BoundKeyVals b p) m) => FetchesAllMemoFetchT b p m

-- | A class indicating the bound type 'b' can be turned into an 'SDP
-- m (BigFloat p)' for any precision 'p', and any 'm' capable of
-- fetching 'SDPFetchKeys b'.
class CanFetchBound b => ToSDP b where
  type SDPFetchKeys b :: [Type]
  toSDP
    :: (KnownNat p, Applicative m, HasForce m, FetchesAll (BoundKeyVals b p) m)
    => b
    -> SDP m (BigFloat p)

-- | A class for computing the 'FetchConfig' and dependency build
-- chain for a given bound 'b'.
class Ord (SumDropVoid (SDPFetchKeys b)) => SDPFetchBuildConfig b where
  sdpFetchConfig
    :: (KnownNat p, MonadIO m)
    => proxy b
    -> proxy' p
    -> BoundFiles
    -> FetchConfig m (ToKeyVals (BigFloat p) (SDPFetchKeys b))

  sdpDepBuildChain
    :: proxy b
    -> BoundConfig
    -> BoundFiles
    -> SomeBuildChain Job (SumDropVoid (SDPFetchKeys b))

  -- | Number of CPUs to use when computing each SDP
  -- PartChunk. 'Nothing' indicates the default value for the 'Job'
  -- moad that runs the computation (usually 1). If computing a
  -- 'PartChunk' takes a lot of memory, one might want to specify more
  -- CPU's so that fewer PartChunk's are computed in parallel.
  sdpPartChunkNumCpus
    :: Bound Int b
    -> SDPB.PartChunk
    -> Maybe Int
  sdpPartChunkNumCpus _ _ = Nothing
