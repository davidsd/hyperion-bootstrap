{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Hyperion.Bootstrap.Bound.Compute
  ( sdpbInputFromBoundFiles
  , defaultBoundFiles
  , keepAllFiles
  , keepOutAndCheckpoint
  , makeSDPBInput
  , makeSDPDir
  , CanBuildSDP
  , computeWithFileTreatment
  , compute
  , compute'
  , computeClean
  , computeClean'
  , computeCleanWithBlockDir
  , cleanFilesWithTreatment
  , continueSDPBCheckpointed
  , continueSDPBCheckpointed'
  , saveTextCheckpoint
  , convertCheckpointToText
  , setFixedTimeLimit
  , getBoundFunctional
  ) where

import Control.Lens                   (Lens', set, view)
import Control.Monad                  (when)
import Control.Monad.IO.Class         (MonadIO, liftIO)
import Data.Binary                    (Binary)
import Data.List                      (stripPrefix)
import Data.Time.Clock                (NominalDiffTime, addUTCTime,
                                       getCurrentTime)
import Data.Vector                    (Vector)
import Hyperion                       (Job)
import Hyperion.Bootstrap.Bound.Build (writeBoundToDirParallel)
import Hyperion.Bootstrap.Bound.Types (Bound (..), BoundConfig (..),
                                       BoundFileTreatment (..), BoundFiles (..),
                                       FileTreatment (..),
                                       SDPFetchBuildConfig (..), ToSDP (..))
import Hyperion.Log                   qualified as Log
import Hyperion.Static                (Static)
import SDPB qualified
import System.Directory               (removePathForcibly)
import System.Environment             (getEnv)
import System.FilePath.Posix          ((</>))
import Type.Reflection                (Typeable)

sdpbInputFromBoundFiles :: BoundFiles -> [FilePath] -> SDPB.Params -> SDPB.Input
sdpbInputFromBoundFiles BoundFiles{..} files params =
  SDPB.Input { jsonFiles = files ,.. }

defaultBoundFiles :: FilePath -> BoundFiles
defaultBoundFiles workDir = BoundFiles
  { jsonDir              = workDir </> "json"
  , blockDir             = workDir </> "blocks"
  , sdpDir               = workDir </> "sdp"
  , outDir               = workDir </> "out"
  , checkpointDir        = workDir </> "ck"
  , initialCheckpointDir = Nothing
  }

keepAllFiles :: BoundFileTreatment
keepAllFiles = MkBoundFileTreatment
  { jsonDirTreatment       = KeepFile
  , blockDirTreatment      = KeepFile
  , sdpDirTreatment        = KeepFile
  , outDirTreatment        = KeepFile
  , checkpointDirTreatment = KeepFile
  }

keepOutAndCheckpoint :: BoundFileTreatment
keepOutAndCheckpoint = keepAllFiles
  { sdpDirTreatment   = RemoveFile
  , blockDirTreatment = RemoveFile
  , jsonDirTreatment  = RemoveFile
  }

type CanBuildSDP b =
  ( Show b
  , Typeable b
  , Static (ToSDP b)
  , Static (Binary b)
  , Static (SDPFetchBuildConfig b)
  )

-- | Write SDP parts to their respective json files and create
-- SDPB.Input.
makeSDPBInput :: CanBuildSDP b => Bound Int b -> BoundFiles -> Job SDPB.Input
makeSDPBInput bound boundFiles = do
  jsonFiles <- writeBoundToDirParallel bound boundFiles
  pure $ sdpbInputFromBoundFiles boundFiles jsonFiles bound.solverParams

-- | Only make the SDPDir used by SDPB (if it doesn't already
-- exist). Does NOT run the solver. This is useful in conjunction with
-- sdp_derivative.
makeSDPDir :: CanBuildSDP b => Bound Int b -> BoundFiles -> Job SDPB.Input
makeSDPDir bound boundFiles = do
  input <- makeSDPBInput bound boundFiles
  let srunPmp2sdpExecutable = scriptsDir (boundConfig bound) </> "srun_pmp2sdp.sh"
  liftIO $ SDPB.runPmp2sdp srunPmp2sdpExecutable input
  pure input

cleanFilesWithTreatment :: MonadIO m => BoundFileTreatment -> BoundFiles -> m ()
cleanFilesWithTreatment treat files = liftIO $
  mapM_ rm $ do
  (dir, RemoveFile) <-
    [ (jsonDir files,       jsonDirTreatment treat)
    , (blockDir files,      blockDirTreatment treat)
    , (sdpDir files,        sdpDirTreatment treat)
    , (outDir files,        outDirTreatment treat)
    , (checkpointDir files, checkpointDirTreatment treat)
    ]
  pure dir
  where
    rm path = do
      Log.info "Removing" path
      removePathForcibly path

-- | Switch paths to use the SSD drive on expanse. WARNING: This
-- function is specific to dsd's account on Expanse and won't work
-- elsewhere!
makeSSD :: BoundFiles -> IO BoundFiles
makeSSD files = do
  currentJobId <- getEnv "SLURM_JOB_ID"
  let
    ssd_dir = "/scratch/dsd/job_" <> currentJobId
    lustre_prefix = "/expanse/lustre/scratch/dsd/temp_project/"
    replaceBaseSsd dir = case stripPrefix lustre_prefix dir of
      Just rest -> ssd_dir </> rest
      Nothing   -> error ("Unexpected directory prefix: " <> dir)
  pure files
    { jsonDir       = replaceBaseSsd files.jsonDir
    , blockDir      = replaceBaseSsd files.blockDir
    , sdpDir        = replaceBaseSsd files.sdpDir
    , outDir        = replaceBaseSsd files.outDir
    , checkpointDir = replaceBaseSsd files.checkpointDir
    }

computeWithFileTreatment
  :: CanBuildSDP b
  => BoundFileTreatment
  -> Bound Int b
  -> BoundFiles
  -> Job SDPB.Output
-- Uncomment out these lines and comment out the next line to use the
-- SSD drive:
-- computeWithFileTreatment treatment bound files' = do
--   files <- liftIO $ makeSSD files'
computeWithFileTreatment treatment bound files = do
  Log.info "Creating json_dir and depedencies" (bound, jsonDir files)
  input <- makeSDPBInput bound files
  output <- liftIO $ SDPB.run
    (scriptsDir (boundConfig bound) </> "srun_pmp2sdp.sh")
    (scriptsDir (boundConfig bound) </> "srun_sdpb.sh")
    input
  Log.info "SDPB result" (bound, output)
  when (SDPB.isFinished output) $
    cleanFilesWithTreatment treatment files
  return output

-- | Creates: block directory, json directory, sdp directory,
-- checkpoint directory, output file. Removes sdp directory only if
-- the solver finishes.
compute' :: CanBuildSDP b => Bound Int b -> BoundFiles -> Job SDPB.Output
compute' = computeWithFileTreatment $
  keepAllFiles { sdpDirTreatment = RemoveFile }

-- | Same as compute', but cleans up block directory and json
-- directory if SDPB finishes (as opposed to hitting maxIterations or
-- maxRuntime). The output file, and checkpoint directory remain.
computeClean' :: CanBuildSDP b => Bound Int b -> BoundFiles -> Job SDPB.Output
computeClean' = computeWithFileTreatment keepOutAndCheckpoint

-- | compute' with defaultBoundFiles for the given 'workDir'
compute :: CanBuildSDP b => Bound Int b -> FilePath -> Job SDPB.Output
compute bound workDir =
  compute' bound (defaultBoundFiles workDir)

-- | computeClean' with defaultBoundFiles for the given 'workDir'
computeClean :: CanBuildSDP b => Bound Int b -> FilePath -> Job SDPB.Output
computeClean bound workDir =
  computeClean' bound (defaultBoundFiles workDir)

-- | Compute a bound using the given block dir, remove the json
-- directory at the end, but DO NOT remove the block directory.
computeCleanWithBlockDir
  :: CanBuildSDP b => Bound Int b -> FilePath -> FilePath -> Job SDPB.Output
computeCleanWithBlockDir bound workDir blockDir =
  computeWithFileTreatment treatment bound boundFiles
  where
    boundFiles = (defaultBoundFiles workDir) { blockDir }
    treatment = keepAllFiles
      { sdpDirTreatment  = RemoveFile
      , jsonDirTreatment = RemoveFile
      }

-- | Run SDPB for 0 iterations and save full solution information to outDir
saveTextCheckpoint :: CanBuildSDP b => Bound Int b -> BoundFiles -> Job FilePath
saveTextCheckpoint bound files = do
  result <- compute' (bound { solverParams = solverParams' }) files
  case SDPB.terminateReason result of
    SDPB.MaxIterationsExceeded -> return (outDir files)
    reason -> error $ "Unexpected terminate reason when saving checkpoint: " ++ show reason
  where
    solverParams' = (solverParams bound)
      { SDPB.writeSolution = SDPB.allSolutionParts
      , SDPB.maxIterations = 0
      }

-- | Convert a binary checkpoint to a text checkpoint. In this
-- function, we recompute the needed conformal blocks and JSON
-- files. This is inefficient, but more convenient because blocks and
-- JSON files are often removed to save space.
convertCheckpointToText
  :: CanBuildSDP b
  => Bound Int b
  -> FilePath
  -> FilePath
  -> Job FilePath
convertCheckpointToText bound binaryCheckpointDir workDir =
  saveTextCheckpoint bound files
  where
    defaultFiles = defaultBoundFiles workDir
    files = defaultFiles
      { initialCheckpointDir = Just binaryCheckpointDir
      , outDir = outDir defaultFiles ++ "_text_ck"
      }

-- | Repeatedly run a computation that results in SDPB.Output until
-- the computation finishes. Keeps track of the total runtime. Here
-- 'input' is used only for logging purposes.
continueSDPBCheckpointed' :: (MonadIO m, Show a) => Lens' r SDPB.Output -> a -> m r -> m r
continueSDPBCheckpointed' outputL input run = go 0
  where
    go startTime = do
      r <- run
      let o@SDPB.Output{..} = view outputL r
      let t = startTime + runtime
      if SDPB.isUnfinished o
        then Log.info ("Running for time " <> Log.showText t <> ". Continuing") (input, o) >> go t
        else return $ set outputL (o { SDPB.runtime = t }) r

continueSDPBCheckpointed :: (MonadIO m, Show a) => a -> m SDPB.Output -> m SDPB.Output
continueSDPBCheckpointed = continueSDPBCheckpointed' id

-- | Set sdpbParams to terminate by time 'd' from the current time
setFixedTimeLimit :: NominalDiffTime -> SDPB.Params -> IO SDPB.Params
setFixedTimeLimit d params = do
  now <- getCurrentTime
  return $ params { SDPB.maxRuntime = SDPB.TerminateByTime (addUTCTime d now) }

getBoundFunctional :: (MonadIO m, RealFloat a) => BoundFiles -> m (Vector a)
getBoundFunctional boundFiles = do
  liftIO $ SDPB.readFunctional (outDir boundFiles)
