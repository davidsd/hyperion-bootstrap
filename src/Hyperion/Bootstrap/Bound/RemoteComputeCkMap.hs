{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TypeFamilies      #-}

module Hyperion.Bootstrap.Bound.RemoteComputeCkMap where

import Bootstrap.Math.AffineTransform          (AffineTransform (..))
import Control.Monad.IO.Class                  (liftIO)
import Control.Monad.Reader                    (asks)
import Data.Aeson                              (FromJSON, ToJSON)
import Data.Binary                             (Binary)
import GHC.TypeNats                            (KnownNat)
import Hyperion                                (Cluster, Static (..), cAp,
                                                cPure, clusterJobOptions,
                                                newWorkDir, ptrAp,
                                                remoteEvalJobM)
import Hyperion.Bootstrap.Bound.Compute        (continueSDPBCheckpointed,
                                                defaultBoundFiles,
                                                keepOutAndCheckpoint)
import Hyperion.Bootstrap.Bound.RemoteCompute  (computeWithTimeLimit)
import Hyperion.Bootstrap.Bound.Types          (Bound (..), BoundFiles (..),
                                                SDPFetchBuildConfig, ToSDP)
import Hyperion.Bootstrap.OPESearch.TrackedMap qualified as TM
import Hyperion.Database                       qualified as DB
import Hyperion.Log                            qualified as Log
import Hyperion.Slurm                          qualified as Slurm
import Linear.V                                (V)
import SDPB qualified
import Type.Reflection                         (Typeable)


-- | A 'CheckpointMap b' is a map from a 'Bound Int b' to the
-- checkpoint file for the "closest" bound already recorded in the
-- CheckpointMap. The definition of closeness depends on the type of
-- TrackedMap used, see 'newCheckpointMap'.
type CheckpointMap b = TM.TrackedMap Cluster (Bound Int b) FilePath

-- | Create a CheckpointMap where closeness between two bounds c1 and
-- c2 is determined by the distance between (toVec c1) and (toVec c2),
-- after applying the inverse of the given affine transformation. This
-- is intended to be used in conjunction with a DelaunaySearch, where
-- the same affine transformation determines distance in the Delaunay
-- search and in the CheckpointMap.
newCheckpointMap
 :: (KnownNat n, Typeable b, FromJSON b, ToJSON b, Ord b)
 => AffineTransform n Rational
 -> (Bound Int b -> V n Rational)
 -> Maybe FilePath
 -> Cluster (CheckpointMap b)
newCheckpointMap affine toVec mCheckpoint =
 liftIO (TM.affineLocalityMap affine toVec mCheckpoint) >>=
 TM.mkPersistent (DB.KeyValMap "checkpoints")

-- | Compute a bound using a CheckpointMap to choose the initial
-- checkpoint. When a worker is acquired, query the checkpoint map to
-- find a checkpoint from a completed computation for a nearby point,
-- and use that as the initial checkpoint (it will get copied and not
-- overwritten). When the bound is finished, add the new checkpoint to
-- the CheckpointMap.
remoteComputeWithCheckpointMap
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  => CheckpointMap b
  -> Bound Int b
  -> Cluster SDPB.Output
remoteComputeWithCheckpointMap checkpointMap bound = do
  workDir <- newWorkDir bound
  jobTime <- asks (Slurm.time . clusterJobOptions)
  let
    -- Has initialCheckpointDir = Nothing
    defaultFiles = defaultBoundFiles workDir

    -- The new checkpoint file that will be created when the bound is
    -- evaluated.
    workingCheckpoint = checkpointDir defaultFiles

  result <-
    continueSDPBCheckpointed (workDir, solverParams bound) $
    remoteEvalJobM $ do
    -- This computation will not run until a worker is acquired, due
    -- to the semantics of 'remoteEvalJobM'. We delay running so
    -- that we can use the most recent results from 'checkpointMap'.
    mInitialCheckpoint <- TM.get checkpointMap bound
    let files = defaultFiles { initialCheckpointDir = mInitialCheckpoint }
    pure $ static computeWithTimeLimit `ptrAp`
      closureDict `cAp`
      cPure keepOutAndCheckpoint `cAp`
      cPure jobTime `cAp`
      cPure bound `cAp`
      cPure files

  Log.info "Computed" (bound, result)
  DB.insert (DB.KeyValMap "computations") bound result
  -- Insert the new checkpoint into 'checkpointMap' so that it can be
  -- used by nearby points.
  TM.set checkpointMap bound workingCheckpoint
  return result

-- | A 'CheckpointMap b' is a map from a 'Bound Int b' to the
-- checkpoint file for the "closest" bound already recorded in the
-- CheckpointMap. The definition of closeness depends on the type of
-- TrackedMap used, see 'newCheckpointMap'.
type LambdaMap b l = TM.TrackedMap Cluster (Bound Int b) l

-- | Create a CheckpointMap where closeness between two bounds c1 and
-- c2 is determined by the distance between (toVec c1) and (toVec c2),
-- after applying the inverse of the given affine transformation. This
-- is intended to be used in conjunction with a DelaunaySearch, where
-- the same affine transformation determines distance in the Delaunay
-- search and in the CheckpointMap.
newLambdaMap
 :: (KnownNat n, Typeable b, FromJSON b, ToJSON b, Ord b, FromJSON l, ToJSON l, Typeable l)
 => AffineTransform n Rational
 -> (Bound Int b -> V n Rational)
 -> Maybe l
 -> Cluster (LambdaMap b l)
newLambdaMap affine toVec mLambda =
 liftIO (TM.affineLocalityMap affine toVec mLambda) >>=
 TM.mkPersistent (DB.KeyValMap "lambdas")
