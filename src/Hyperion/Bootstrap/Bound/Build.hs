{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

module Hyperion.Bootstrap.Bound.Build
  ( reifyBound
  , reflectBound
  , sdpPartChunkBuildLink
  , sdpPartBuildLink
  , boundBuildLink
  , boundBuildChain
  , boundToSDPDeps
  , writePartChunk
  , writePart
  , getBoundObject
  , writeBoundToDirParallel
  ) where

import Bootstrap.Build                (BuildLink (..), ComputeDependencies,
                                       FetchesAll, HasForce,
                                       SomeBuildChain (..), SumDropVoid,
                                       buildAndFetch, buildSomeChain,
                                       dependencies, runMemoFetchT, (<:<))
import Control.Exception              qualified as Exception
import Control.Monad.Catch            (MonadCatch, handle)
import Control.Monad.IO.Class         (MonadIO, liftIO)
import Control.Monad.Reader           (local)
import Data.Binary                    (Binary)
import Data.Set                       qualified as Set
import Data.Time                      (diffUTCTime, getCurrentTime)
import GHC.TypeNats                   (KnownNat)
import Hyperion                       (Dict (..), Job, NumCPUs (..),
                                       Static (..), cAp, cPure, remoteEval,
                                       setTaskCpus)
import Hyperion.Bootstrap.Bound.Types (Bound (..), BoundFiles (..),
                                       BoundKeyVals, Precision,
                                       SDPFetchBuildConfig (..), ToSDP (..),
                                       reflectPrecision, reifyPrecision)
import Hyperion.Log                   qualified as Log
import Hyperion.Util                  (minute)
import Hyperion.Util.MapMonitored     (mapConcurrentlyMonitored)
import SDPB                           (BigFloat)
import SDPB qualified
import System.Directory               (createDirectoryIfMissing, doesPathExist)
import Type.Reflection                (Typeable)

-- | Turn a Bound with precision stored as an 'Int' into a bound with
-- precision stored a the type level as 'Precision p'
reifyBound :: Bound Int b -> (forall p . KnownNat p => Bound (Precision p) b -> r) -> r
reifyBound bound go =
  reifyPrecision bound.precision $ \p -> go (bound { precision = p })

-- | Convert type-level precision back to an 'Int'
reflectBound :: KnownNat p => Bound (Precision p) b -> Bound Int b
reflectBound bound = bound { precision = reflectPrecision bound.precision }

boundToSDP
  :: ( ToSDP b
     , KnownNat p
     , Applicative f
     , HasForce f
     , FetchesAll (BoundKeyVals b p) f
     )
  => Bound (Precision p) b
  -> SDPB.SDP f (BigFloat p)
boundToSDP = toSDP . boundKey

boundToSDPDeps
  :: (ToSDP b, KnownNat p)
  => Bound (Precision p) b
  -> SDPB.SDP (ComputeDependencies (BoundKeyVals b p)) (BigFloat p)
boundToSDPDeps = toSDP . boundKey

checkFilesCreated :: MonadIO m => (k -> [FilePath]) -> k -> m Bool
checkFilesCreated toFiles key =
  liftIO $ and <$> mapM doesPathExist (toFiles key)

sdpPartChunkBuildLink
  :: ( Static (ToSDP b)
     , Static (SDPFetchBuildConfig b)
     , Static (Binary b)
     , Typeable b
     , KnownNat p
     )
  => Bound (Precision p) b
  -> BoundFiles
  -> BuildLink Job SDPB.PartChunk (SumDropVoid (SDPFetchKeys b))
sdpPartChunkBuildLink bound boundFiles = BuildLink
  { buildDeps    = SDPB.withPartChunk sdp dependencies
  , checkCreated = checkFilesCreated (pure . SDPB.partChunkPath dir)
  , buildAll = \partChunkSet -> do
      liftIO $ createDirectoryIfMissing True dir
      mapConcurrentlyMonitored "SDP PartChunk's" (2*minute)
        (remoteWritePartChunk bound boundFiles)
        (Set.toList partChunkSet)
  }
  where
    sdp = boundToSDPDeps bound
    dir = jsonDir boundFiles
    remoteWritePartChunk bnd files partChunk =
      let
        setCpus = case sdpPartChunkNumCpus (reflectBound bnd) partChunk of
          Nothing   -> id
          Just cpus -> local (setTaskCpus (NumCPUs cpus))
      in
        setCpus $
        remoteEval $
        static writePartChunk
        `cAp` closureDict
        `cAp` cPure bnd
        `cAp` cPure files
        `cAp` cPure partChunk

-- | By default, we memoize the fetch function (which in general reads
-- and parses BlockTableKey's) in writePartChunk. Note that
-- runMemoFetchT makes a memoization plan by determining dependencies
-- and then only keeping memoized results in memory as long as they
-- will be needed. Thus, unlike naive memoization, runMemoFetchT
-- shouldn't increase the memory usage too much unless many objects
-- are needed multiple times, with big overlaps between their time
-- intervals.
writePartChunk
  :: forall b p m. (MonadIO m, MonadCatch m)
  => Dict (ToSDP b, SDPFetchBuildConfig b, KnownNat p)
  -> Bound (Precision p) b
  -> BoundFiles
  -> SDPB.PartChunk
  -> m ()
writePartChunk Dict bound boundFiles partChunk =
  handle (Log.throw @m @Exception.SomeException) $ do
  startTime <- liftIO getCurrentTime
  Log.info "Writing PartChunk" (partChunk, SDPB.partChunkPath (jsonDir boundFiles) partChunk)
  let cfg = sdpFetchConfig bound bound.precision boundFiles
  () <- liftIO =<<
    runMemoFetchT
    (SDPB.encodePartChunk (jsonDir boundFiles) partChunk (boundToSDP bound))
    cfg
  endTime <- liftIO getCurrentTime
  Log.info "Finished writing PartChunk" (diffUTCTime endTime startTime)

sdpPartBuildLink
  :: ( Static (ToSDP b)
     , Static (Binary b)
     , Typeable b
     , KnownNat p
     )
  => Bound (Precision p) b
  -> BoundFiles
  -> BuildLink Job SDPB.Part SDPB.PartChunk
sdpPartBuildLink bound boundFiles = BuildLink
  { buildDeps = SDPB.partDependencies sdp
  , checkCreated = checkFilesCreated (pure . SDPB.partPath dir)
  , buildAll = \partSet -> do
      liftIO $ createDirectoryIfMissing True dir
      mapConcurrentlyMonitored "SDP Part's" (2*minute)
        (remoteWritePart bound boundFiles)
        (Set.toList partSet)
  }
  where
    sdp = boundToSDPDeps bound
    dir = jsonDir boundFiles
    remoteWritePart bnd files part =
      remoteEval $
      static writePart
      `cAp` closureDict
      `cAp` cPure bnd
      `cAp` cPure files
      `cAp` cPure part

writePart
  :: forall b p m. (MonadIO m, MonadCatch m)
  => Dict (ToSDP b, KnownNat p)
  -> Bound (Precision p) b
  -> BoundFiles
  -> SDPB.Part
  -> m ()
writePart Dict bound boundFiles sdpPart =
  handle (Log.throw @m @Exception.SomeException) $ do
  let sdp = boundToSDPDeps bound
  Log.info "Writing" (SDPB.partPath (jsonDir boundFiles) sdpPart)
  liftIO (SDPB.encodePart (jsonDir boundFiles) sdpPart sdp)

boundBuildLink :: (MonadIO m, KnownNat p, ToSDP b) => Bound (Precision p) b -> BuildLink m () SDPB.Part
boundBuildLink bound =
  let sdp = boundToSDPDeps bound
  in BuildLink
     { buildDeps = const (SDPB.parts sdp)
     , checkCreated = const (pure False)
     , buildAll = const (pure ())
     }

boundBuildChain
  :: ( Static (ToSDP b)
     , Static (SDPFetchBuildConfig b)
     , Static (Binary b)
     , Typeable b
     , KnownNat p
     )
  => Bound (Precision p) b
  -> BoundFiles
  -> SomeBuildChain Job ()
boundBuildChain bound boundFiles =
  boundBuildLink bound <:<
  sdpPartBuildLink bound boundFiles <:<
  sdpPartChunkBuildLink bound boundFiles <:<
  sdpDepBuildChain bound (boundConfig bound) boundFiles

getBoundObject
  :: ( SDPFetchBuildConfig b
     , ToSDP b
     , KnownNat p
     )
  => Bound (Precision p) b
  -> BoundFiles
  -> (forall f . (Applicative f, HasForce f, FetchesAll (BoundKeyVals b p) f) => b -> f r)
  -> Job r
getBoundObject bound boundFiles toObject =
  buildAndFetch
    (sdpDepBuildChain bound (boundConfig bound) boundFiles)
    (sdpFetchConfig bound bound.precision boundFiles)
    (toObject (boundKey bound))

writeBoundToDirParallel
  :: ( Typeable b
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  => Bound Int b
  -> BoundFiles
  -> Job [FilePath]
writeBoundToDirParallel bound' f = reifyBound bound' $ \bound -> do
  let files = map (SDPB.partPath (jsonDir f)) (SDPB.parts (boundToSDPDeps bound))
  buildSomeChain (boundBuildChain bound f) ()
  pure files
