{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TypeFamilies      #-}

module Hyperion.Bootstrap.Bound.RemoteBinarySearch where

import Control.Distributed.Process      (unClosure)
import Control.Monad                    (when)
import Control.Monad.IO.Class           (liftIO)
import Control.Monad.Reader             (asks, lift)
import Data.Aeson                       (ToJSON (..), object, (.=))
import Data.Binary                      (Binary)
import Data.BinaryHash                  (hashBase64Safe)
import Data.Time                        (NominalDiffTime, addUTCTime,
                                         getCurrentTime)
import GHC.Generics                     (Generic)
import Hyperion                         (Closure, Cluster, Dict (..), Job,
                                         Static (..), cAp, cPtr, cPure,
                                         clusterJobOptions, getObjectId,
                                         newWorkDir, objectIdToText, ptrAp,
                                         remoteEvalJob)
import Hyperion.Bootstrap.BinarySearch  (BinarySearchConfig (..), Bracket (..),
                                         binarySearchPersistent)
import Hyperion.Bootstrap.Bound.Compute (cleanFilesWithTreatment,
                                         computeWithFileTreatment,
                                         defaultBoundFiles,
                                         keepOutAndCheckpoint,
                                         setFixedTimeLimit)
import Hyperion.Bootstrap.Bound.Types   (Bound (..), BoundFileTreatment (..),
                                         BoundFiles (..), FileTreatment (..),
                                         SDPFetchBuildConfig, ToSDP)
import Hyperion.Database                qualified as DB
import Hyperion.Log                     qualified as Log
import Hyperion.Slurm                   qualified as Slurm
import Hyperion.Static.Orphans          ()
import SDPB qualified
import System.Directory                 (removePathForcibly)
import Type.Reflection                  (Typeable)


data BinarySearchBound b = MkBinarySearchBound
  { bsBoundClosure      :: Closure (Rational -> Bound Int b)
  , bsResultBoolClosure :: ResultBoolClosure b
  , bsConfig            :: BinarySearchConfig Rational
  } deriving (Show, Generic, Binary)

-- | Turn the result of a bound computation into a Bool. We allow an
-- arbitrary computation in the Job monad, which can be useful as a
-- hook for additional processing.
type ResultBoolClosure b = Closure (Bound Int b -> BoundFiles -> SDPB.Output -> Job Bool)

instance Typeable b => Static (Binary (BinarySearchBound b)) where
  closureDict = cPtr (static Dict)

data HotStartingRule = HotStarting | NoHotStarting
  deriving (Show, Generic, Binary, Eq)

instance Static (Binary HotStartingRule) where closureDict = cPtr (static Dict)

-- | This ToJSON instance is not designed for round-tripping. Instead,
-- it is intended to be a (mostly) unique key labeling the binary
-- search in the database. Hence the use of hashBase64Safe, as opposed
-- to storing the full Closure in JSON. We do NOT provide a FromJSON
-- instance.
instance Typeable b => ToJSON (BinarySearchBound b) where
  toJSON b = object
    [ "bsBoundClosure" .= hashBase64Safe (bsBoundClosure b)
    , "bsConfig" .= toJSON (bsConfig b)
    ]

-- | The default hook is to do nothing and return whether the result
-- is not dual feasible.
defaultResultBoolClosure :: Typeable b => ResultBoolClosure b
defaultResultBoolClosure = cPtr (static (\_ _ result -> pure (not (SDPB.isDualFeasible result))))

remoteBinarySearchBound
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  => BinarySearchBound b
  -> Cluster (Bracket Rational)
remoteBinarySearchBound =
  remoteBinarySearchBoundWithFileTreatment keepOutAndCheckpoint defaultBoundFiles

remoteBinarySearchBoundWithHotStarting
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  => BinarySearchBound b
  -> Cluster (Bracket Rational)
remoteBinarySearchBoundWithHotStarting =
  remoteBinarySearchBoundWithFileTreatmentAndHotStarting HotStarting keepOutAndCheckpoint defaultBoundFiles

remoteBinarySearchBoundWithFileTreatment
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  => BoundFileTreatment
  -> (FilePath -> BoundFiles)
  -> BinarySearchBound b
  -> Cluster (Bracket Rational)
remoteBinarySearchBoundWithFileTreatment = remoteBinarySearchBoundWithFileTreatmentAndHotStarting NoHotStarting

remoteBinarySearchBoundWithFileTreatmentAndHotStarting
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  => HotStartingRule
  -> BoundFileTreatment
  -> (FilePath -> BoundFiles)
  -> BinarySearchBound b
  -> Cluster (Bracket Rational)
remoteBinarySearchBoundWithFileTreatmentAndHotStarting rule bsFileTreatment mkBoundFiles bs = do
  maybeResult <- DB.lookup binarySearchRationalBrackets bs
  case maybeResult of
    Just result -> return result
    Nothing -> do
      workDir <- newWorkDir bs
      jobTime <- asks (Slurm.time . clusterJobOptions)
      let
        files = mkBoundFiles workDir
        go = do
          mResult <- remoteEvalJob $
            static computeBinarySearch `ptrAp`
            closureDict `cAp`
            cPure jobTime `cAp`
            cPure bs `cAp`
            cPure bsFileTreatment `cAp`
            cPure files `cAp`
            cPure rule
          case mResult of
            Nothing -> do
              Log.info "Search in progress; continuing" bs
              go
            Just bracket -> pure bracket
      bracket <- go
      mkBound <- lift $ unClosure (bsBoundClosure bs)
      let boundBracket = fmap mkBound bracket
      Log.info "Computed" boundBracket
      DB.insert binarySearchBoundBrackets bs boundBracket
      DB.insert binarySearchRationalBrackets bs bracket
      cleanFilesWithTreatment bsFileTreatment files
      return bracket
  where
    -- The purpose of binarySearchRationalBrackets is to allow resuming
    -- binary search computations if they are killed for some reason.
    binarySearchRationalBrackets = DB.KeyValMap "binary_search_rational_brackets"
    -- The purpose of binarySearchBoundBrackets is to be a list of
    -- human-useful resuts. The key will contain an ugly Closure hash,
    -- but the result contains the two bounds that bracket the
    -- boundary between false and true.
    binarySearchBoundBrackets = DB.KeyValMap "binary_searches"

computeBinarySearch
  :: Dict
     ( Show b
     , Typeable b
     , ToJSON b
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  -> NominalDiffTime
  -> BinarySearchBound b
  -> BoundFileTreatment
  -> BoundFiles
  -> HotStartingRule
  -> Job (Maybe (Bracket Rational))
computeBinarySearch Dict jobTime bs bsFileTreatment boundFiles rule = do
  -- We ask SDPB and the binary search to terminate within 90% of
  -- the jobTime. This should ensure sufficiently prompt exit as
  -- long as an iteration doesn't take 10% or more of the jobTime.
  --
  -- Currently, this functionality DOES NOT WORK for binary search
  -- -- it will keep going until its job ends.
  now <- liftIO getCurrentTime
  let binarySearchCfg = (bsConfig bs)
        { terminateTime = Just (addUTCTime (0.9*jobTime) now) }
  mkCftBound <- lift $ unClosure (bsBoundClosure bs)
  getResultBool <- lift $ unClosure (bsResultBoolClosure bs)
  -- We compute the solverParams by evaluating mkCftBound at
  -- 0. This may not be a physically sensible value of the
  -- argument, but we assume the solverParams are ok.
  solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams (mkCftBound 0))
  searchObjId <- getObjectId bs
  let
    testGap gap = do
      let
        bound = (mkCftBound gap) { solverParams = solverParams' }
        -- Probably, sharing the block directory and json directory
        -- for all the binary search steps is good for efficiency. We
        -- would have to revisit this decision if we binary-searched
        -- along a direction that involved generating a billion new
        -- block files.
        treatment = bsFileTreatment
          { blockDirTreatment = KeepFile
          , jsonDirTreatment  = KeepFile
          }
      -- Using checkpoints with an optimization, as in the
      -- TwistGap case, does not work well
      when (rule == NoHotStarting) $ liftIO (removePathForcibly (checkpointDir boundFiles))
      result <- computeWithFileTreatment treatment bound boundFiles
      Log.info "Binary search step" (bound, gap, result)
      DB.insert (DB.KeyValMap "computations") bound result
      getResultBool bound boundFiles result
    individualSearchKVMap = DB.KeyValMap ("binary_search_" <> objectIdToText searchObjId)
  binarySearchPersistent individualSearchKVMap binarySearchCfg testGap
