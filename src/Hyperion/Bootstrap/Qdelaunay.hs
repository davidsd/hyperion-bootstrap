{-# LANGUAGE OverloadedStrings #-}

module Hyperion.Bootstrap.Qdelaunay where

import Control.Monad                     (forM_)
import Data.Attoparsec.ByteString.Char8  hiding (IResult (..))
import Data.ByteString.Char8             qualified as BS
import Data.Double.Conversion.ByteString (toExponential)
import Data.Vector                       (Vector)
import Data.Vector                       qualified as V
import GHC.IO.Handle                     (hClose)
import Hyperion.Log                      qualified as Log
import System.Process                    (CreateProcess (..), StdStream (..),
                                          proc, withCreateProcess)

qdelaunay :: FilePath -> Vector (Vector Double) -> IO [Vector Int]
qdelaunay executable points =
  withCreateProcess qProc $ \stdin stdout _ _ ->
    case (stdin, stdout) of
      (Just hin, Just hout) -> do
        writeInput hin
        hClose hin
        out <- BS.hGetContents hout
        case parseOnly (qdelaunayOutput dim) out of
          Right r -> return r
          Left e  -> Log.throwError $ "Could not parse qdelaunay output: " ++ e
      _ -> Log.throwError "Couldn't create pipe to qdelaunay process"
  where
    qProc = (proc executable ["Qt", "i"])
      { std_in  = CreatePipe
      , std_out = CreatePipe
      }
    writeInput h = do
      BS.hPutStrLn h (BS.pack (show dim))
      BS.hPutStrLn h (BS.pack (show numPoints))
      forM_ points $ \v ->
        BS.hPutStrLn h (BS.intercalate " " (V.toList (V.map (toExponential 16) v)))
    numPoints = V.length points
    dim = V.length (points V.! 0)

qdelaunayOutput :: Int -> Parser [Vector Int]
qdelaunayOutput dim = do
  numSimplices <- decimal <* endOfLine
  count numSimplices (simplex <* endOfLine)
  where
    simplex = V.replicateM (dim+1) (decimal <* " ")
