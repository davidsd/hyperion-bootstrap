{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeApplications    #-}

module Hyperion.Bootstrap.BinarySearch where

import           Control.Monad.Catch                 (MonadCatch)
import           Control.Monad.IO.Class              (MonadIO)
import           Control.Monad.Reader                (MonadReader)
import           Data.Aeson                          (FromJSON, ToJSON)
import           Data.Binary                         (Binary)
import           Data.Binary.Instances.Time          ()
import           Data.List                           (maximumBy, sortOn)
import qualified Data.List.NonEmpty                  as NonEmpty
import qualified Data.Map                            as Map
import           Data.Ord                            (comparing)
import           Data.Time.Clock                     (UTCTime)
import           GHC.Generics                        (Generic)
import qualified Bootstrap.Math.AffineTransform  as AT
import qualified Hyperion.Bootstrap.PointCloudSearch as PC
import           Hyperion.Concurrent                 (Concurrently)
import qualified Hyperion.Database                   as DB
import           Hyperion.Static                     (Dict (..), Static (..),
                                                      ptrAp)
import           Linear.V                            (V)
import           Bootstrap.Math.Linear.Literal            (fromV, toV)
import           Bootstrap.Math.Linear.Util               ()
import           Type.Reflection                     (Typeable)

-- | A bracket between two values. We are polymorphic in 'a' because
-- Bracket will be used with 'a ~ Rational' for input and 'a ~ V 1
-- Rational' for the PointCloudSearch (which operates with vectors)
data Bracket a = Bracket
  { truePoint  :: a -- ^ Value for which function is known to be true
  , falsePoint :: a -- ^ Value for which function is known to be false
  } deriving (Show, Generic, Binary, FromJSON, ToJSON, Functor)

instance (Typeable a, Static (Binary a)) => Static (Binary (Bracket a)) where
  closureDict = static (\Dict -> Dict) `ptrAp` closureDict @(Binary a)

-- | Configuration for a binary search
data BinarySearchConfig a = BinarySearchConfig
  { initialBracket :: Bracket a -- ^ Assume the search value is within
                                -- this Bracket
  , threshold      :: a         -- ^ Stop the search when window has
                                -- size smaller than threshold
  , terminateTime  :: Maybe UTCTime
  } deriving (Show, Generic, Binary, FromJSON, ToJSON)

-- | The number of binary search steps needed to reduce the Bracket to
-- size 'threshold'
bracketSteps :: (Fractional a, Real a) => a -> Bracket a -> Int
bracketSteps threshold b =
  ceiling $ log @Double (realToFrac (abs (falsePoint b - truePoint b) / threshold)) / log 2

bracketSize :: Bracket (V 1 Rational) -> Rational
bracketSize (Bracket p q) = abs (fromV (p - q))

-- | A "point map" of type @Map (V n Rational (Maybe Bool)@ is used by
-- 'PointCloudSearch'. Here we construct one from a Bracket.
bracketToPointMap :: Bracket Rational -> Map.Map (V 1 Rational) (Maybe Bool)
bracketToPointMap b = Map.fromList
  [ (toV (truePoint b),  Just True)
  , (toV (falsePoint b), Just False)
  ]

-- | Note that this works both for a ~ Rational and a ~ V 1 Rational,
-- due to the Num and Fractional instances for V 1 a.
bracketMid :: Fractional a => Bracket a -> a
bracketMid (Bracket p q) = (p + q)/2

-- | Largest bracket between adjacent values that disagree. TODO: When
-- this was originally written, it made sense to have multiple
-- adjacent values that disagree. Now it doesn't really make
-- sense. Replace maximumBy with something?
pointMapToBracket :: Map.Map (V 1 Rational) (Maybe Bool) -> Maybe (Bracket (V 1 Rational))
pointMapToBracket pointMap =
  maximumBy (comparing bracketSize) <$> NonEmpty.nonEmpty brackets
  where
    points = sortOn (fromV . fst) (Map.toList pointMap)
    -- | Pairs of adjacent points who's Bools disagree
    brackets = do
      -- Note that this works even if 'points' is null
      ((p1, b1), (p2, b2)) <- zip points (drop 1 points)
      case (b1, b2) of
        (Just True, Just False) -> [Bracket {truePoint = p1, falsePoint = p2}]
        (Just False, Just True) -> [Bracket {truePoint = p2, falsePoint = p1}]
        _                       -> []

-- | TODO: If we run out of time, we just return Nothing right
-- now. Make the return value more meaningful.
binarySearchPersistent
  :: ( MonadIO m, MonadReader env m, DB.HasDB env, MonadCatch m
     , Applicative (Concurrently m)
     )
  => DB.KeyValMap (V 1 Rational) (Maybe Bool)
  -> BinarySearchConfig Rational
  -> (Rational -> m Bool)
  -> m (Maybe (Bracket Rational))
binarySearchPersistent resultKVMap cfg f = do
  status <- PC.pointCloudSearchRegionPersistent resultKVMap pointCloudCfg affine initialPts (f . fromV @1)
  case status of
    PC.Finished -> do
      finalBracket <- fromPointMap . Map.fromList <$> DB.lookupAll resultKVMap
      pure (Just finalBracket)
    PC.Incomplete -> pure Nothing
  where
    initialPts = bracketToPointMap (initialBracket cfg)
    pointCloudCfg = PC.PointCloudConfig
      { PC.nThreads        = 1
      , PC.nSteps          = bracketSteps (threshold cfg) . fromPointMap
      , PC.getNextPoint    = pure . fmap bracketMid . pointMapToBracket
      , PC.addBoundingCube = False
      , PC.terminateTime   = terminateTime cfg
      }
    -- The affine transformation doesn't matter in 1 dimension
    affine = AT.AffineTransform (toV 0) (toV (toV 1))
    fromPointMap = maybe (initialBracket cfg) (fmap fromV) . pointMapToBracket

