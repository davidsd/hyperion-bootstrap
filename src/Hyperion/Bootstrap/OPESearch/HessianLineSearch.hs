{-# OPTIONS_GHC -fplugin GHC.TypeLits.KnownNat.Solver #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}

module Hyperion.Bootstrap.OPESearch.HessianLineSearch where

import Bootstrap.Math.BoolFunction                qualified as B
import Bootstrap.Math.Linear                      (outer, (.*))
import Bootstrap.Math.Linear                      qualified as L
import Bootstrap.Math.VectorSpace                 qualified as VS
import Control.Exception                          (Exception, throw, try)
import Data.List.NonEmpty                         (NonEmpty (..), (<|))
import Data.List.NonEmpty                         qualified as NE
import Data.Matrix.Static                         (Matrix)
import Data.Matrix.Static                         qualified as M
import Data.Maybe                                 (fromMaybe)
import Data.Proxy                                 (Proxy (..))
import GHC.TypeNats
import Hyperion.Bootstrap.OPESearch.BilinearForms (quadraticPositive)
import Hyperion.Log                               qualified as Log
import Linear.Metric                              (norm)
import Linear.V                                   (V)
import Linear.Vector                              ((*^), (^/))
import Numeric.Eigen.Static                       (eigensystemSymmetric,
                                                   luSolve)
import Numeric.LinearAlgebra                      qualified as LA
import Numeric.Rounded                            (Precision, Rounded, Rounding,
                                                   toDouble)
import QuadraticNet.Math                          (fromHVector)
import System.Random                              (randomIO, randomRIO)

data HessianLineSearchError
  = PositiveSegmentHasNoEndpoint
  | UnexpectedBoolFunction String
    deriving (Show, Exception)

-- Hessian and gradient of the potential -(1/alpha) (x^T Q x / x^T x)^alpha
-- When alpha = 0, this is equivalent to -log (x^T Q x / x^T x)
hessianAndGradientTerm
  :: (KnownNat j, Floating a)
  => Int
  -> V j a
  -> Matrix j j a
  -> (Matrix j j a, V j a)
hessianAndGradientTerm alpha x q = (h, g)
  where
    qx = q .* x
    xqx = x .* qx
    xx = x .* x
    pre = (xqx/xx)^alpha
    h0 =
      (-2/xqx) *^ q
      + (4/(xqx*xqx)) *^ qx `outer` qx
      + (2/xx) *^ M.identity
      - (4/(xx*xx)) *^ x `outer` x
    g0 = ((2/xx) *^ x) - ((2/xqx) *^ qx)
    h = pre *^ (h0 + ((fromIntegral alpha) *^ g0 `outer` g0))
    g = pre *^ g0

hessianAndGradient
  :: (KnownNat j, Floating a)
  => Int
  -> V j a
  -> NonEmpty (Matrix j j a)
  -> (Matrix j j a, V j a)
hessianAndGradient alpha x qForms =
  let (hs, gs) = NE.unzip (fmap (hessianAndGradientTerm alpha x) qForms)
  in (VS.sum hs, VS.sum gs)

hessian :: (KnownNat j, Floating a) => Int -> V j a -> NonEmpty (Matrix j j a) -> Matrix j j a
hessian alpha x qForms = fst (hessianAndGradient alpha x qForms)

gradient :: (KnownNat j, Floating a) => Int -> V j a -> NonEmpty (Matrix j j a) -> V j a
gradient alpha x qForms = snd (hessianAndGradient alpha x qForms)

randomVector :: forall j a . (KnownNat j, Fractional a) => IO (V j a)
randomVector = do
  seed <- randomIO
  return $ fmap realToFrac $ fromHVector $ LA.randomVector seed LA.Gaussian $ fromIntegral (natVal @j Proxy)

randomTangentVector :: (KnownNat j, Fractional a) => V j a -> IO (V j a)
randomTangentVector x = do
  v <- randomVector
  let proj = ((v .* x) / (x .* x)) *^ x
  return (v - proj)

normConstrained :: forall j a . (KnownNat j, 1 <= j, Num a) => V j a -> Matrix j j a -> Matrix (j+1) (j+1) a
normConstrained x h = M.joinBlocks
  ( h                 , 2 *^ L.colVector x
  , 2 *^ L.rowVector x, M.zero
  )

constrainedSolve
  :: forall j r p . (KnownNat j, 1 <= j, Rounding r, Precision p)
  => V j (Rounded r p)
  -> Matrix j j (Rounded r p)
  -> V j (Rounded r p)
  -> V j (Rounded r p)
constrainedSolve x h v =
  L.init $ luSolve (normConstrained x h) (L.snoc v 0)

halfConstrainedSolve
  :: forall j r p . (KnownNat j, 1 <= j, Rounding r, Precision p)
  => V j (Rounded r p)
  -> Matrix j j (Rounded r p)
  -> V j (Rounded r p)
  -> V j (Rounded r p)
halfConstrainedSolve x h v =
  L.init $ halfSolveSymmetric (normConstrained x h) (L.snoc v 0)

halfSolveSymmetric
  :: (KnownNat j, Rounding r, Precision p)
  => Matrix j j (Rounded r p)
  -> V j (Rounded r p)
  -> V j (Rounded r p)
halfSolveSymmetric a x =
  let (ls, vs) = eigensystemSymmetric a
  in vs .* liftA2 (\l z -> signum l * z / sqrt (abs l)) ls (M.transpose vs .* x)

randomHessianDirection
  :: (KnownNat j, 1 <= j, Rounding r, Precision p)
  => NonEmpty (Matrix j j (Rounded r p))
  -> V j (Rounded r p)
  -> IO (V j (Rounded r p))
randomHessianDirection qForms x = do
  tangent <- randomTangentVector x
  return $ halfConstrainedSolve x (hessian 0 x qForms) tangent

lineQuadraticPositive
  :: (KnownNat j, Floating a, Ord a)
  => V j a
  -> V j a
  -> Matrix j j a
  -> B.BoolFunction a
lineQuadraticPositive x xTangent q = quadraticPositive m
  where
    m = L.toM ((a, b), (b, d))
    d = xTangent .* q .* xTangent
    b = xTangent .* q .* x
    a = x .* q .* x

-- | Gives the point a fraction segmentFraction along the positive segment
-- through x in the direction of xTangent.
pointPositiveSegment
  :: (KnownNat j, Ord a, Floating a, Show a)
  => a
  -> V j a
  -> V j a
  -> NonEmpty (Matrix j j a)
  -> V j a
pointPositiveSegment segmentFraction x xTangent qForms =
  let allowed = B.getAll $ foldMap (B.MkAll . lineQuadraticPositive x xTangent) qForms
      (x1, x2) = case (B.initial allowed, B.switchPoints allowed) of
        (False, a:b:_) ->
          ( L.normalize (x + (a *^ xTangent))
          , L.normalize (x + (b *^ xTangent))
          )
        (True, a:ss) ->
          case reverse ss of
            b:_ ->
              ( L.normalize (x + (a *^ xTangent))
              , (-1) *^ L.normalize (x + (b *^ xTangent))
              )
            [] -> throw PositiveSegmentHasNoEndpoint
        -- Hit this issue in ozVqW-yATve
        _ -> throw $ UnexpectedBoolFunction (show (allowed,segmentFraction,x,xTangent))
  -- Apparently, when very close to the boundary of the allowed
  -- region, this can sometimes return a disallowed point. The
  -- function should probably check whether the result is allowed and
  -- return a Maybe value.
  in L.normalize (segmentFraction *^ x1 + (1-segmentFraction) *^ x2)

data LineSearchType
  = MidPoint
  | RandomPoint

getSegmentFraction :: Fractional a => LineSearchType -> IO a
getSegmentFraction MidPoint    = return 0.5
getSegmentFraction RandomPoint = realToFrac <$> randomRIO @Double (0,1)

hessianLineSearch
  :: (KnownNat j, 1 <= j, Rounding r, Precision p)
  => LineSearchType
  -> NonEmpty (Matrix j j (Rounded r p))
  -> V j (Rounded r p)
  -> Int
  -> IO (NonEmpty (V j (Rounded r p)))
hessianLineSearch ty qForms xInitial nSteps = go (pure xInitial) nSteps
  where
    go xs 0 = return xs
    go xs@(x:|_) n = do
      v <- randomHessianDirection qForms x
      gamma <- getSegmentFraction ty
      let x' = pointPositiveSegment gamma x v qForms
      go (x' <| xs) (n-1)

hessianLineSearchAverage
  :: (KnownNat j, 1 <= j, Rounding r, Precision p)
  => LineSearchType
  -> NonEmpty (Matrix j j (Rounded r p))
  -> V j (Rounded r p)
  -> Int
  -> Int
  -> IO (V j (Rounded r p))
hessianLineSearchAverage ty qForms xInitial nSteps nAverage = do
  xs <- hessianLineSearch ty qForms xInitial nSteps
  Log.info ("Hessian line search finished. Last distances") (nAverage, distances (NE.take nAverage xs))
  let mx = do
        xs' <- NE.nonEmpty (NE.take nAverage xs)
        let x = VS.sum xs' ^/ fromIntegral nAverage
        if isFeasible x qForms
          then return x
          else Nothing
  return $ fromMaybe (NE.head xs) mx
  where
    distances xs = zipWith dist xs (drop 1 xs)
    dist x x' = norm (fmap toDouble x' - fmap toDouble x)

isFeasible :: (KnownNat j, Ord a, Fractional a) => V j a -> NonEmpty (Matrix j j a) -> Bool
isFeasible x qForms = all (\q -> x .* q .* x > 0) qForms

takeFeasibleStep
  :: (KnownNat j, Fractional a, Ord a)
  => Int
  -> NonEmpty (Matrix j j a)
  -> V j a
  -> V j a
  -> Maybe (V j a)
takeFeasibleStep maxTests qForms x step = go maxTests step
  where
    go 0 _ = Nothing
    go n s =
      let x' = x + s
      in if isFeasible x' qForms
         then Just x'
         else go (n-1) (s ^/ 2)

descentSearchDirection
  :: (KnownNat j, 1 <= j, Rounding r, Precision p)
  => NonEmpty (Matrix j j (Rounded r p))
  -> V j (Rounded r p)
  -> Matrix j j (Rounded r p)
  -> Rounded r p
  -> V j (Rounded r p)
descentSearchDirection qForms x q tilt =
  constrainedSolve x (h + hq) ((-1) *^ (g + gq))
  where
    (h,g) = hessianAndGradient 0 x qForms
    (hq, gq) = hessianAndGradientTerm 1 x (tilt *^ q)

data DescentConfig = DescentConfig
  { maxDescentSteps       :: Int
  , maxDescentRuns        :: Int
  , randomLineSearchSteps :: Int
  , tiltRatio             :: Double
  , tiltInitial           :: Double
  , maxFeasibleStepTests  :: Int
  }

defaultDescentConfig :: DescentConfig
defaultDescentConfig = DescentConfig
  { maxDescentSteps       = 5000
  , maxDescentRuns        = 20
  , randomLineSearchSteps = 50
  , tiltRatio             = 1.03
  , tiltInitial           = 1.0
  , maxFeasibleStepTests  = 100
  }

data DescentResult a
  = Feasible Int Double a
  | Infeasible
  deriving (Functor, Show)

descentSearch
  :: (KnownNat j, 1 <= j, Rounding r, Precision p)
  => DescentConfig
  -> NonEmpty (Matrix j j (Rounded r p))
  -> Matrix j j (Rounded r p)
  -> V j (Rounded r p)
  -> DescentResult (V j (Rounded r p))
descentSearch DescentConfig{..} qForms q xInitial =
  go tiltInitial maxDescentSteps xInitial
  where
    go _ 0 _ = Infeasible
    go t n x | x .* q .* x > 0 = Feasible (maxDescentSteps - n) t x
    go t n x =
      let dx = descentSearchDirection qForms x q (realToFrac t)
      in case takeFeasibleStep maxFeasibleStepTests qForms x dx of
        Just x' -> go (t*tiltRatio) (n-1) (L.normalize x')
        Nothing -> Infeasible

descentSearchRandomized
  :: (KnownNat j, 1 <= j, Rounding r, Precision p)
  => DescentConfig
  -> NonEmpty (Matrix j j (Rounded r p))
  -> Matrix j j (Rounded r p)
  -> V j (Rounded r p)
  -> IO (DescentResult (V j (Rounded r p)))
descentSearchRandomized c@DescentConfig{..} qForms q xInitial = do
  Log.info "Starting descent search with data" ( map M.toLists (NE.toList qForms)
                                               , M.toLists q
                                               , xInitial)
  go maxDescentRuns xInitial
  where
    go 0 _ = return Infeasible
    go n x = case descentSearch c qForms q x of
      Infeasible -> do
        either_x' <- try $ NE.head <$> hessianLineSearch RandomPoint qForms x randomLineSearchSteps
        case either_x' of
          Right x' -> do
            Log.info "Descent search failed. Trying new point" x'
            go (n-1) x'
          -- TODO: We can apparently get an error from
          -- hessianLineSearch if the tangent direction shrinks to
          -- near zero. In this case, we currently terminate the
          -- descent search. There might be a better approach -- maybe
          -- we should try again a few times?
          Left (e :: HessianLineSearchError) -> do
            Log.info "hessianLineSearch failure" e
            Log.text "Terminating descent search."
            return Infeasible
      r -> return r
