{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}

module Hyperion.Bootstrap.OPESearch.TrackedMap where

import           Control.Concurrent.STM             (atomically, modifyTVar,
                                                     newTVarIO, readTVarIO)
import           Control.Monad.Catch                (MonadCatch)
import           Control.Monad.IO.Class             (MonadIO, liftIO)
import           Control.Monad.Reader               (MonadReader)
import           Data.Aeson                         (FromJSON, ToJSON)
import           Data.List.Extra                    (minimumOn)
import qualified Data.Map                           as Map
import           Data.Typeable                      (Typeable)
import           GHC.TypeNats                       (KnownNat)
import qualified Bootstrap.Math.AffineTransform as AT
import qualified Hyperion.Database                  as DB
import qualified Linear.Metric                      as L
import           Linear.V                           (V)

-- | A map from keys to values where the get and set operations can
-- involve a monadic computation (e.g. backing up the map to a
-- database).
data TrackedMap m a b = TrackedMap
  { get :: a -> m (Maybe b)
  , set :: a -> b -> m ()
  }

-- | A tracked map that always returns a given value
constTrackedMap :: Monad m => Maybe b -> TrackedMap m a b
constTrackedMap c = TrackedMap
  { get = \_ -> return c
  , set = \_ _ -> return ()
  }

-- | A tracked map that returns the value associated to the key with
-- the closest distance to the queried key
localityMap :: (Ord a, MonadIO m) => (a -> a -> Double) -> Maybe b -> IO (TrackedMap m a b)
localityMap distance defaultVal = do
  pointMapVar <- newTVarIO Map.empty
  let
    get a = liftIO $ do
      pointMap <- readTVarIO pointMapVar
      return $ case Map.toList pointMap of
        [] -> defaultVal
        xs -> Just $ snd $ minimumOn (distance a . fst) xs
    set a b = liftIO $ atomically $ modifyTVar pointMapVar (Map.insert a b)
  return TrackedMap{..}

-- | The distance between to objects after applying the inverse of the
-- given affine transformation.
affineDistance
  :: (KnownNat n)
  => AT.AffineTransform n Rational
  -> (a -> V n Rational)
  -> a
  -> a
  -> Double
affineDistance affine toVector a a' =
  L.distance (toV a) (toV a')
  where
    inv = AT.apply (AT.inverse affine)
    toV = fmap fromRational . inv . toVector

-- | A localityMap that measures distance using affineDistance
affineLocalityMap
  :: (Ord a, MonadIO m, KnownNat n)
  => AT.AffineTransform n Rational
  -> (a -> V n Rational)
  -> Maybe b
  -> IO (TrackedMap m a b)
affineLocalityMap affine toVector =
  localityMap (affineDistance affine toVector)

-- | Make a TrackedMap persist to the database. Upon instantiation,
-- the map is populated with any key/value pairs already in the
-- database. Each time a new pair is set, it is inserted into the
-- database.
mkPersistent
  :: ( ToJSON a, FromJSON a, Typeable a, ToJSON b, FromJSON b, Typeable b
     , MonadIO m, MonadReader env m, DB.HasDB env, MonadCatch m
     )
  => DB.KeyValMap a b
  -> TrackedMap m a b
  -> m (TrackedMap m a b)
mkPersistent collection tMap = do
  dbElements <- DB.lookupAll collection
  mapM_ (uncurry (set tMap)) dbElements
  let
    set' a b = do
      DB.insert collection a b
      set tMap a b
  return tMap { set = set' }
