{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE StaticPointers        #-}

module Hyperion.Bootstrap.OPESearch.Remote where

import Control.Monad.Extra                        (whenJust)
import Control.Monad.Reader                       (asks)
import Data.Aeson                                 (ToJSON)
import Data.Binary                                (Binary)
import Data.Data                                  (Typeable)
import GHC.TypeNats                               (KnownNat)
import Hyperion
import Hyperion.Bootstrap.Bound                   (Bound (..),
                                                   SDPFetchBuildConfig, ToSDP)
import Hyperion.Bootstrap.OPESearch.BilinearForms (BilinearForms (..),
                                                   setFeasibleVector)
import Hyperion.Bootstrap.OPESearch.Run           (bilinearFormsMap,
                                                   opeNetSearch,
                                                   opeSearchCheckpointMap)
import Hyperion.Bootstrap.OPESearch.TrackedMap    (TrackedMap (..))
import Hyperion.Bootstrap.OPESearch.Types         (OPESearchConfig (..),
                                                   OPESearchData (..),
                                                   OPESearchResult (..))
import Hyperion.Database                          qualified as DB
import Hyperion.Log                               qualified as Log
import Hyperion.Slurm                             qualified as Slurm
import Linear.V                                   (V)

remoteOPESearch
  :: ( KnownNat j
     , Static (Show b)
     , Static (Binary b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (SDPFetchBuildConfig b)
     , Typeable b
     )
  => Closure (OPESearchConfig b j)
  -> TrackedMap Cluster (Bound Int b) FilePath
  -> TrackedMap Cluster (Bound Int b) (V j Rational)
  -> BilinearForms j
  -> Bound Int b
  -> Cluster Bool
remoteOPESearch cfgClosure checkpointMap lambdaMap initialBilinearForms' bound =
  go getInitialSearchData
  where
    go getSearchData = do
      -- We use remoteEvalJobM here so that getSearchData is only
      -- evaluated when a worker is acquired. It is useful to evaluate
      -- getSearchData as late as possible to maximize the chances of
      -- getting a good checkpoint from checkpointMap.
      result <- remoteEvalJobM $ do
        searchData <- getSearchData
        pure $
          static opeNetSearch `ptrAp`
          closureDict `cAp`
          cfgClosure `cAp`
          cPure searchData
      Log.info "Computed" (bound, result)
      -- We save the checkpoint file for future use at the end of a
      -- run. We don't save it if the status is SearchingPoint. Thus,
      -- we don't have to worry about race conditions for intermediate
      -- checkpoint files. Checkpoints will only be reused once they
      -- are done being written to.
      case result of
        Disallowed _ mCheckpoint -> do
          whenJust mCheckpoint (set checkpointMap bound)
          return False
        FoundAllowedPoint _ mCheckpoint lambda -> do
          set lambdaMap bound lambda
          whenJust mCheckpoint (set checkpointMap bound)
          return True
        SearchingPoint searchData -> go (return searchData)

    getInitialSearchData = do
      -- We set maxDuration to 90% of jobMaxTime, to allow sufficient time
      -- for cleanup.  TODO: Is there a better way to handle this? When
      -- jobMaxTime is small compared to the time for an iteration, then
      -- the job is in danger of being killed.
      maxDuration <- asks ((0.9*) . Slurm.time . clusterJobOptions)
      -- This code block is run once per master startup. (Due to the fact
      -- that remoteEvalJobM caches the result of getSearchData.) We
      -- memoize the result so that (get checkpointMap bound) gets
      -- run at most once over the course of the whole computation. We
      -- should still check the database in Run.hs, in case a
      -- RemoteEvalError occurs after the checkpoint is updated and the
      -- computation is re-run.
      initialCheckpoint <- DB.memoizeWithMap opeSearchCheckpointMap (get checkpointMap) bound
      workDir <- newWorkDir bound
      -- Again, we memoize this so that (get lambdaMap b) is run at most
      -- once over the whole computation. We must still check the database
      -- in Run.hs for the same reason as above.
      initialBilinearForms <- flip (DB.memoizeWithMap bilinearFormsMap) bound $ \b -> do
        initialLambda <- get lambdaMap b
        return $ setFeasibleVector initialLambda initialBilinearForms'
      programInfo <- asks clusterProgramInfo
      return OPESearchData{..}
