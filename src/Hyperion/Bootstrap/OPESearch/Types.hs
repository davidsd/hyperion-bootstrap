{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StaticPointers #-}

module Hyperion.Bootstrap.OPESearch.Types where

import Bootstrap.Build                            (FetchesAll, HasForce)
import Data.Binary                                (Binary (..))
import Data.Matrix.Static                         (Matrix)
import Data.Time.Clock                            (NominalDiffTime)
import Data.Vector                                (Vector)
import GHC.Generics                               (Generic)
import GHC.TypeNats                               (KnownNat)
import Hyperion                                   (Dict (..), ProgramInfo,
                                                   Static (..), cAp)
import Hyperion.Bootstrap.Bound                   (Bound, BoundKeyVals)
import Hyperion.Bootstrap.OPESearch.BilinearForms (BilinearForms)
import Linear.V                                   (V)
import SDPB                                       (BigFloat)
import Type.Reflection                            (Typeable)

type CheckpointPath = FilePath

data OPESearchResult b j
  = SearchingPoint (OPESearchData b j)
  | FoundAllowedPoint (BilinearForms j) (Maybe CheckpointPath) (V j Rational)
  | Disallowed (BilinearForms j) (Maybe CheckpointPath)
  deriving (Show, Eq, Generic, Binary)

instance (KnownNat j, Typeable b, Static (Binary b)) => Static (Binary (OPESearchResult b j)) where
  closureDict = static (\Dict Dict -> Dict) `cAp`
                closureDict @(KnownNat j) `cAp`
                closureDict @(Binary b)

data OPESearchConfig b j = OPESearchConfig
  { setOPE :: forall prec . V j Rational -> Bound prec b -> Bound prec b
  , toOPEMatrix
    :: forall f p .
       ( Applicative f
       , HasForce f
       , KnownNat p
       , FetchesAll (BoundKeyVals b p) f
       )
    => b
    -> f (Matrix j j (Vector (BigFloat p)))
  , queryAllowed :: BilinearForms j -> IO (Maybe (V j Rational))
  }

data OPESearchData b j = OPESearchData
  { bound                :: Bound Int b
  , workDir              :: FilePath
  , initialCheckpoint    :: Maybe FilePath
  , initialBilinearForms :: BilinearForms j
  , programInfo          :: ProgramInfo
  , maxDuration          :: NominalDiffTime
  } deriving (Show, Eq, Generic, Binary)

instance (KnownNat j, Typeable b, Static (Binary b)) => Static (Binary (OPESearchData b j)) where
  closureDict = static (\Dict Dict -> Dict) `cAp`
                closureDict @(KnownNat j) `cAp`
                closureDict @(Binary b)
