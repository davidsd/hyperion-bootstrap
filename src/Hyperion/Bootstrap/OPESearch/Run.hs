{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiWayIf            #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Hyperion.Bootstrap.OPESearch.Run where

import Bootstrap.Math.Util                        (dot)
import Control.Concurrent.Memoize                 (memoize)
import Control.Monad                              (when)
import Control.Monad.IO.Class                     (liftIO)
import Data.Aeson                                 (ToJSON)
import Data.Binary                                (Binary)
import Data.Matrix.Static                         (Matrix)
import Data.Ratio                                 (approxRational)
import Data.Set                                   qualified as Set
import Data.Time.Clock                            (UTCTime, addUTCTime,
                                                   getCurrentTime)
import Data.Vector                                (Vector)
import GHC.TypeNats                               (KnownNat, Nat)
import Hyperion                                   (Dict (..), Job, Static)
import Hyperion.Bootstrap.Bound                   (Bound (..),
                                                   BoundFileTreatment (..),
                                                   BoundFiles (..), CanBuildSDP,
                                                   FileTreatment (..),
                                                   Precision,
                                                   SDPFetchBuildConfig,
                                                   ToSDP (..),
                                                   computeWithFileTreatment,
                                                   defaultBoundFiles,
                                                   getBoundObject, keepAllFiles,
                                                   reflectBound, reifyPrecision)
import Hyperion.Bootstrap.OPESearch.BilinearForms (BilinearForms (..),
                                                   addBilinearForm,
                                                   feasibleVector,
                                                   setFeasibleVector)
import Hyperion.Bootstrap.OPESearch.Types         (OPESearchConfig (..),
                                                   OPESearchData (..),
                                                   OPESearchResult (..))
import Hyperion.Database                          qualified as DB
import Hyperion.Log                               qualified as Log
import Hyperion.Util                              (hashTruncateFileName,
                                                   sanitizeFileString)
import Linear.V                                   (V)
import Numeric.Rounded                            (Rounded, RoundingMode (..))
import SDPB qualified
import System.Directory                           (createDirectoryIfMissing,
                                                   removePathForcibly)
import System.FilePath.Posix                      ((</>))
import Type.Reflection                            (Typeable)

bounds :: DB.KeyValMap (Bound Int b) SDPB.Output
bounds = DB.KeyValMap "computations"

boundOutFiles :: DB.KeyValMap (Bound Int b) FilePath
boundOutFiles = DB.KeyValMap "outFiles"

bilinearFormsMap :: DB.KeyValMap (Bound Int b) (BilinearForms j)
bilinearFormsMap = DB.KeyValMap "bilinearForms"

opeSearchCheckpointMap :: DB.KeyValMap (Bound Int b) (Maybe FilePath)
opeSearchCheckpointMap = DB.KeyValMap "opeSearchCheckpoint"

saveBilinearForms :: (KnownNat j, ToJSON b, KnownNat p) => Bound (Precision p) b -> BilinearForms j -> Job ()
saveBilinearForms bound forms =
  DB.insert bilinearFormsMap (reflectBound bound) forms

saveResult :: forall (p :: Nat) b . (ToJSON b, KnownNat p) => Bound (Precision p) b -> BoundFiles -> SDPB.Output -> Job ()
saveResult bound files result = do
  DB.insert bounds bound' result
  DB.insert boundOutFiles bound' (outDir files)
  where
    bound' = reflectBound @p bound

opeNetSearch
  :: forall b j .
     Dict
     ( KnownNat j
     , Show b
     , Typeable b
     , ToJSON b
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     )
  -> OPESearchConfig b j
  -> OPESearchData b j
  -> Job (OPESearchResult b j)
opeNetSearch Dict config searchData = do
  Log.info "OPE search" (bound searchData, workDir searchData)
  endTime <- liftIO $ fmap (addUTCTime (maxDuration searchData)) getCurrentTime

  -- We want to save the path to the new checkpoint file precisely
  -- once, after the first run of SDPB finishes. Thus, we memoize this
  -- call.
  saveCheckpointPath :: FilePath -> Job () <- liftIO $ memoize $
    DB.insert opeSearchCheckpointMap (bound searchData) . Just

  -- It is possible that opeNetSearch could be retried in case of a
  -- RemoteEvalError. Thus, we should check the database in case
  -- initialCheckpoint and initialBilinearForms need to be updated.
  initialCheckpoint' <-
    DB.lookupDefault opeSearchCheckpointMap (initialCheckpoint searchData) (bound searchData)
  initialBilinearForms' <-
    DB.lookupDefault bilinearFormsMap (initialBilinearForms searchData) (bound searchData)

  liftIO $ createDirectoryIfMissing True (workDir searchData)
  let defaultFiles = defaultBoundFiles (workDir searchData)
  reifyPrecision (precision (bound searchData)) $
    runOPESearch endTime saveCheckpointPath defaultFiles config
    (searchData { initialCheckpoint    = initialCheckpoint'
                , initialBilinearForms = initialBilinearForms'
                })

setEndTime :: UTCTime -> Bound prec b -> Bound prec b
setEndTime t b =
  b { solverParams = (solverParams b) { SDPB.maxRuntime = SDPB.TerminateByTime t } }

setWriteDualVector :: Bound prec b -> Bound prec b
setWriteDualVector b = b
  { solverParams = (solverParams b)
    { SDPB.writeSolution = Set.insert SDPB.DualVector_y (SDPB.writeSolution (solverParams b)) }
  }

runOPESearch
  :: forall p b j . (KnownNat j, KnownNat p, CanBuildSDP b, ToJSON b)
  => UTCTime
  -> (FilePath -> Job ())
  -> BoundFiles
  -> OPESearchConfig b j
  -> OPESearchData b j
  -> Precision p
  -> Job (OPESearchResult b j)
runOPESearch endTime saveCheckpointPath defaultFiles searchConfig searchData prec = do
  opeMatrix <- getBoundObject cftB defaultFiles (toOPEMatrix searchConfig)
  go opeMatrix (initialCheckpoint searchData) (initialBilinearForms searchData)
  where
    cftB = (bound searchData) { precision = prec }
    workCheckpoint = checkpointDir defaultFiles
    resolution = realToFrac (bfResolution (initialBilinearForms searchData))

    go
      :: Matrix j j (Vector (Rounded 'TowardZero p))
      -> Maybe FilePath
      -> BilinearForms j
      -> Job (OPESearchResult b j)
    go opeMatrix maybeCheckpoint bilinearForms = case feasibleVector bilinearForms of
      Just lambda -> do
        saveBilinearForms cftB bilinearForms
        searchWithLambda opeMatrix lambda maybeCheckpoint bilinearForms
      Nothing -> liftIO (queryAllowed searchConfig bilinearForms) >>= \case
        Just lambda -> do
          let newBilinearForms = setFeasibleVector (Just lambda) bilinearForms
          saveBilinearForms cftB newBilinearForms
          searchWithLambda opeMatrix lambda maybeCheckpoint newBilinearForms
        Nothing     -> do
          Log.text "All lambdas disallowed."
          saveBilinearForms cftB bilinearForms
          return (Disallowed bilinearForms maybeCheckpoint)

    searchWithLambda
      :: Matrix j j (Vector (Rounded 'TowardZero p))
      -> V j Rational
      -> Maybe FilePath
      -> BilinearForms j
      -> Job (OPESearchResult b j)
    searchWithLambda opeMatrix lambda maybeCheckpoint bilinearForms = do
      let cftBLambda =
            setWriteDualVector .
            setEndTime endTime .
            setOPE searchConfig lambda $
            cftB
          lambdaFile = hashTruncateFileName (sanitizeFileString ("lambda=" ++ show lambda))
          files = defaultFiles
            { outDir = (workDir searchData) </> lambdaFile ++ "_out"
            , initialCheckpointDir = maybeCheckpoint
            }
      -- Automatically remove the sdp directory when finished
      result <-
        computeWithFileTreatment
        (keepAllFiles { sdpDirTreatment = RemoveFile })
        (reflectBound cftBLambda)
        files
      saveCheckpointPath workCheckpoint
      when (SDPB.isFinished result) (saveResult cftBLambda files result)
      if | SDPB.isPrimalFeasible result -> do
             -- cleanup blockDir and jsonDir. At this point, the only
             -- remaining files should be outDir and checkpointDir
             liftIO $ mapM_ removePathForcibly [blockDir files, jsonDir files]
             Log.info "Found allowed lambda" lambda
             return (FoundAllowedPoint bilinearForms (Just workCheckpoint) lambda)
         | SDPB.isDualFeasible result -> do
             alpha <- liftIO $ SDPB.readFunctional (outDir files)
             let m = flip approxRational resolution . dot alpha <$> opeMatrix
             go opeMatrix (Just workCheckpoint) (addBilinearForm m bilinearForms)
         | SDPB.isUnfinished result -> do
             let currentData = searchData
                   { initialCheckpoint = Just workCheckpoint
                   , initialBilinearForms = bilinearForms
                   }
             return (SearchingPoint currentData)
         | otherwise -> Log.throwError $ "Unexpected SDPB output:" ++ show result
