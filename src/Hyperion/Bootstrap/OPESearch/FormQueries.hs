{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE TypeFamilies        #-}
{-# LANGUAGE TypeOperators       #-}

-- | A FormQuery is a procedure to take 'BilinearForms' and produce a
-- vector on which all the given quadratic forms are simultaneously
-- negative (if one exists). The idea is that a run of SDPB will
-- produce a quadratic form on OPE space, and when this quadratic form
-- is positive, the given OPE vector is ruled out. Thus, a new OPE
-- vector to test must be negaive when paired with all current
-- quadratic forms.
--
-- 'queryAllowedBoolFunction' is rigorous, since we can analytically
-- compute the allowed region for a collection of 2x2 quadratic
-- forms. However, the others are non-rigorous, and in general work
-- best when chained together in 'queryAllowedMixed'.

module Hyperion.Bootstrap.OPESearch.FormQueries where

import Bootstrap.Math.BoolFunction                    qualified as B
import Control.Applicative                            ((<|>))
import Control.Monad.Catch                            (SomeException, catch)
import Control.Monad.Except                           (runExceptT)
import Control.Monad.IO.Class                         (MonadIO)
import Control.Monad.Trans.Maybe                      (MaybeT (..), runMaybeT)
import Data.List.NonEmpty                             (NonEmpty (..), (<|))
import Data.List.NonEmpty                             qualified as NE
import Data.Matrix.Static                             qualified as M
import Data.Proxy                                     (Proxy)
import Data.Ratio                                     (approxRational)
import Data.Reflection                                (reifyNat)
import Fmt                                            ((+||), (||+))
import GHC.TypeNats
import Hyperion.Bootstrap.OPESearch.BilinearForms     (BilinearForms (..),
                                                       midMaxTrueInterval,
                                                       quadraticPositive)
import Hyperion.Bootstrap.OPESearch.HessianLineSearch (DescentConfig (..),
                                                       DescentResult (..),
                                                       LineSearchType (..),
                                                       descentSearchRandomized,
                                                       hessianLineSearchAverage)
import Hyperion.Log                                   qualified as Log
import Linear.V                                       (V)
import QuadraticNet                                   qualified as QN
import SDPB                                           (BigFloat)

-- | Given a collection of 2x2 quadratic forms, find a vector on which
-- they are simultaneously negative, if one exists (Nothing
-- otherwise). For each form, we compute a BoolFunction corresponding
-- to its negative intervals. Then we 'and' the BoolFunction's
-- together, and then take the midpoint of the largest True interval.
queryAllowedBoolFunction :: MonadIO m => BilinearForms 2 -> m (Maybe (V 2 Rational))
queryAllowedBoolFunction (BilinearForms res fs) = do
  Log.info "queryAllowedBoolFunction: Allowed range" allowed
  Log.info "queryAllowedBoolFunction: Choosing point" allowedMidpoint
  pure $ fmap (fmap floatToRational) allowedMidpoint
  where
    -- TODO: Make precision configurable. For now, 448 binary digits
    -- should be good enough.
    toFloatMatrix :: M.Matrix 2 2 Rational -> M.Matrix 2 2 (BigFloat 448)
    toFloatMatrix = fmap fromRational

    floatToRational :: BigFloat 448 -> Rational
    floatToRational x = approxRational x (realToFrac res)

    -- Negate the matrices because we want to find a region where all
    -- quadratic forms are negative.
    matrixNegativeBoolFunction = quadraticPositive . negate . toFloatMatrix
    allowed = B.getAll $ foldMap (B.MkAll . matrixNegativeBoolFunction . snd) fs
    allowedMidpoint = midMaxTrueInterval allowed

-- | Given a collection of jxj quadratic forms, find a vector on which
-- they are simultneously negative, if one exists (Nothing
-- otherwise). We negate the matrices and then use quadratic-net to
-- find a location on which they are all positive. Note that
-- quadratic-net is not guaranteed to succeed or produce prove that
-- there is no solution.
queryAllowedQuadraticNet
  :: forall j . (KnownNat j, KnownNat (j-1))
  => QN.QuadraticNetConfig
  -> Double
  -> BilinearForms j
  -> IO (Maybe (V j Rational))
queryAllowedQuadraticNet qNetConfig resolution (BilinearForms _ fs) =
  reifyNat (fromIntegral qNetConfig.precision) $
    \(_ :: Proxy p) -> do
      -- We negate the matrices because we want to find a region where
      -- all quadratic forms are negative. We rescale so that various
      -- thresholds defined in QuadraticNet work correctly.
      let qForms :: NE.NonEmpty (M.Matrix j j (BigFloat p)) =
            rescaleNegateMatrix <$> NE.fromList (map snd fs)
      vMaybe <- noteError (QN.runSearch qNetConfig qForms)
      return $ fmap (fmap (`approxRational` realToFrac resolution)) vMaybe
  where
    noteError m = runExceptT m >>= \case
      Left e -> Log.info "QuadraticNet error" e >> return Nothing
      Right r -> return r

rescaleMatrix :: (KnownNat j, Fractional a) => M.Matrix j j a -> M.Matrix j j a
rescaleMatrix m = fmap (/ scale) m
  where
    scale = sum (map abs (M.toList m)) / fromIntegral (M.ncols m * M.nrows m)

rescaleNegateMatrix :: (KnownNat j, Fractional a) => M.Matrix j j Rational -> M.Matrix j j a
rescaleNegateMatrix = rescaleMatrix . negate . fmap realToFrac

-- | Given a collection of jxj quadratic forms, together with a vector
-- on which all but one of them are negative, try to find a new vector
-- on which all of them are negative. We do this by trying to decrease
-- the value of the remaining quadratic form, while staying inside the
-- negative region of the other forms. In general, this will only work
-- if the negative region is sufficiently nice (e.g. convex). We also
-- try multiple times from different starting points.
queryAllowedDescent
  :: forall j . (KnownNat j, 1 <= j)
  => DescentConfig
  -> Int
  -> Double
  -> Int
  -> Int
  -> BilinearForms j
  -> IO (Maybe (V j Rational))
queryAllowedDescent descentConfig prec resolution hessianLineSteps hessianLineAverage (BilinearForms _ qs) =
  reifyNat (fromIntegral prec) $
  \(_ :: Proxy p) -> case qs of
    (_, qNewR) : (Just lambdaPrevR, qForm'R) : qFormsR -> do
      let lambdaPrev :: V j (BigFloat p) = fmap realToFrac lambdaPrevR
          qNew = rescaleNegateMatrix qNewR
          qForms = rescaleNegateMatrix <$> qForm'R :| map snd qFormsR
      Log.text $
        "Starting descent search with "+||length qForms + 1||+
        " quadratic forms of size "+||M.nrows qNew||+
        "x"+||M.ncols qNew||+""
      descentSearchRandomized descentConfig qForms qNew lambdaPrev >>= \case
        Feasible n tilt lambda -> do
          Log.info "Descent search succeeded" (n, tilt, lambda)
          lambda' <- hessianLineSearchAverage MidPoint (qNew <| qForms) lambda hessianLineSteps hessianLineAverage
          return (Just (fmap (`approxRational` realToFrac resolution) lambda'))
        Infeasible -> do
          Log.text "Descent search failed."
          return Nothing
    _ -> return Nothing

data QueryMixedConfig = QueryMixedConfig
  { qmDescentConfig      :: DescentConfig
  , qmQuadraticNetConfig :: QN.QuadraticNetConfig
  , qmPrecision          :: Int
  , qmResolution         :: Double
  , qmHessianLineSteps   :: Int
  , qmHessianLineAverage :: Int
  }

-- | Try two strategies to find a vector on which the given quadratic
-- forms are negative. First try descent search. If that fails, fall
-- back on quadratic-net.
queryAllowedMixed
  :: (KnownNat j, KnownNat (j-1), 1 <= j)
  => QueryMixedConfig
  -> BilinearForms j
  -> IO (Maybe (V j Rational))
queryAllowedMixed QueryMixedConfig{..} b =
  runMaybeT (MaybeT tryDescent <|> MaybeT tryQN)
  where
    tryDescent =
      queryAllowedDescent qmDescentConfig qmPrecision qmResolution qmHessianLineSteps qmHessianLineAverage b
      `catch` (\(e :: SomeException) -> Log.info "Descent search error" e >> return Nothing)
    tryQN =
      queryAllowedQuadraticNet qmQuadraticNetConfig qmResolution b
      `catch` (\(e :: SomeException) -> Log.info "QuadraticNet error" e >> return Nothing)
