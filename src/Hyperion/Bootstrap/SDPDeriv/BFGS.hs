{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE OverloadedStrings           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE MultiWayIf          #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TemplateHaskell     #-}
{-# LANGUAGE TypeApplications    #-}

module Hyperion.Bootstrap.SDPDeriv.BFGS
  ( Config(..)
  , defaultConfig
  , IslandExtConfig(..)
  , defaultExtremaConfig
  , BFGSSearchStatus(..)
  , BFGSData(..)
  , run
  , extremizeIsland
  , extremizeIslandSafe
  ) where

import qualified Hyperion.Log as Log
import Control.Monad.IO.Class (MonadIO)
import           Control.Applicative                ((<|>))
import           Data.Aeson                         (FromJSON, ToJSON)
import           Data.Binary                        (Binary)
import           Data.Foldable                      (toList)
import           Data.Maybe                         (fromJust, catMaybes)
import           GHC.Generics                       (Generic)
import           GHC.TypeNats                       (KnownNat)
import qualified Hyperion.Bootstrap.SDPDeriv.Jet2   as Jet2
import           Hyperion.Bootstrap.SDPDeriv.Newton (approxRational')
import           Hyperion.Static                    (Dict (..), Static (..),
                                                     cAp, cPtr)
import           Hyperion.Static.Orphans            ()
import           Linear.Metric                      (dot, norm, signorm)
import           Linear.V                           (V)
import           Type.Reflection                    (Typeable)

import qualified Bootstrap.Math.Linear              as L
import           Data.Matrix.Static                 (Matrix)
import qualified Linear.Matrix                      as L
import qualified Linear.Metric                      as L
import           Linear.Vector                      ((*^), (^/))

data Config n = MkConfig
  { -- | Small deviation used when computing the gradient and hessian
    epsGradHess         :: Rational
    -- | Resolution used when converting numerical result for Newton step to Rational
  , stepResolution      :: Rational
    -- | Terminate the Newton search when the step size gets smaller than this amount.
  , gradNormThreshold   :: Rational
    -- | If true, the search terminates at the first negative point found
  , stopOnNegative      :: Bool
    -- | Minimum vertex of the bounding box
  , boundingBoxMin      :: V n Rational
    -- | Maximum vertex of the bounding box
  , boundingBoxMax      :: V n Rational
    -- | Hessian approximation to use when starting the search (will be
    -- |   calculated from bounding box if not given)
  , initialHessianGuess :: Maybe (Matrix n n Rational)
  } deriving (Eq, Ord, Show, Generic, Binary)

instance (KnownNat n) => Static (Binary (Config n)) where
  closureDict = cPtr (static (\Dict -> Dict)) `cAp` closureDict @(KnownNat n)

defaultConfig :: V n Rational -> V n Rational -> Config n
defaultConfig bbMin bbMax = MkConfig
  { epsGradHess         = 1e-9
  , stepResolution      = 1e-32
  , gradNormThreshold   = 1e-9
  , stopOnNegative      = True
  , boundingBoxMin      = bbMin
  , boundingBoxMax      = bbMax
  , initialHessianGuess = Nothing
  }

data IslandExtConfig n = MkIslandExtConfig
  { -- | Params for underlying BFGS update
    bfgsConfig         :: Config n
    -- | Termination threshold for objective
  , goalToleranceObj   :: Rational
    -- | Termination threshold for perpendicular part of gradient
  , goalToleranceGrad  :: Rational
    -- | Factor to shrink the objective by in each iteration (need not be <1)
  , lineSearchDecrease :: Rational
    -- | Bound on number of line searches to do. If lineSearchDecrease >= 1,
    -- | convergence is not guaranteed, so this should be set
  , maxIters           :: Maybe Int
  } deriving (Eq, Ord, Show, Generic, Binary)

instance (KnownNat n) => Static (Binary (IslandExtConfig n)) where
  closureDict = cPtr (static (\Dict -> Dict)) `cAp` closureDict @(KnownNat n)

defaultExtremaConfig :: Config n -> IslandExtConfig n
defaultExtremaConfig solverConfig = MkIslandExtConfig
  { bfgsConfig         = solverConfig
  , goalToleranceObj   = 1e-6
  , goalToleranceGrad  = 1e-3
  , lineSearchDecrease = 0.8
  , maxIters           = Nothing
  }

data BFGSSearchStatus = Success | BoundaryExceeded | EvaluationError
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

data BFGSData n a = MkBFGSData
  { x            :: V n Rational
  , constant     :: a
  , gradient     :: V n a
  , hessianGuess :: Maybe (Matrix n n a)
  , searchStatus :: BFGSSearchStatus
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

instance (KnownNat n, Typeable a, Static (Binary a)) => Static (Binary (BFGSData n a)) where
  closureDict = cPtr (static (\Dict Dict -> Dict)) `cAp`
                closureDict @(KnownNat n) `cAp`
                closureDict @(Binary a)

-- | Given a monadic function `getValueAndGradient` that takes a precision
-- and a point and returns a BFGSData object containing the value of
-- the objective and its gradient (and optionally its Hessian), performs
-- a BFGS descent starting from 'xInitial'
run
  :: (KnownNat n, RealFrac a, Floating a, Show a, MonadIO m)
  => Config n
  -> (Rational -> V n Rational -> m (BFGSData n a))
  -> V n Rational
  -> m (V n Rational, [BFGSData n a]) -- TODO: change tuple to data?
run cfg@MkConfig{..} getValueAndGradient xInitial =
  do
    initialPointData <- getValueAndGradient epsGradHess xInitial
    allPoints <- go [withHessianGuess cfg initialPointData]
    pure (x (head allPoints), allPoints)
  where
    -- Flow control and stop condition/bounds checking
    -- (keeping the whole list of points is probably not needed, but could make
    --  it easy to troubleshoot in the future)
    go pointList@(currentData@MkBFGSData{..}:_)
      | stopOnNegative && constant < 0                 = pure pointList
      | norm gradient < fromRational gradNormThreshold = pure pointList
      | searchStatus == BoundaryExceeded               = pure pointList
      | otherwise = do
          Log.info "Doing BFGS step with current data" currentData
          nextPoint <- bfgsStep cfg getValueAndGradient (head pointList)
          Log.info "Got BFGS next point" nextPoint
          go (nextPoint : pointList)
    -- quash warning (should never call this with empty list of points)
    go [] = undefined

-- | Given a monadic function `getValueAndGradient`, the BFGS output
-- for an initial point, and a direction to extremize in, finds the
-- tip of the region where the objective is negative.
-- Based on Algorithm 2 in arXiv:2104.09518
extremizeIsland
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => IslandExtConfig n
  -> (Rational -> V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> m (V n Rational, [BFGSData n a])
extremizeIsland MkIslandExtConfig{..} getValueAndGradient initialPointData direction =
  go 1 [initialPointData] initialPointData
  where
    -- testing of end condition
    endCondition constant gradient = let
      gtol = fromRational goalToleranceObj
      gtolD = fromRational goalToleranceGrad
      projected = L.project (fromRational <$> direction) gradient
      in (abs constant <= gtol) && (norm (gradient - projected) <= gtolD)
    -- Main flow control
    -- lastBFGS is the point at which the hessian was most recently updated
    go iter pointList@(point:_) lastBFGS
      | fmap (<iter) maxIters == Just True             = pure (x point, pointList)
      | endCondition (constant point) (gradient point) = pure (x point, pointList)
      | otherwise = do
          let stepRes = stepResolution bfgsConfig
              xExtremum = extremizeEllipsoid lastBFGS (fromRational <$> direction)
              stepDirection = fmap (approxRational' stepRes) xExtremum
                              - (x point)
          pointNew <- interpolatingLineSearch
                      (getValueAndGradient (epsGradHess bfgsConfig))
                      point stepDirection stepRes lineSearchDecrease
--          pointNew <- bisectingLineSearch
--                      (getValueAndGradient (epsGradHess bfgsConfig))
--                      point stepDirection lineSearchDecrease
          -- hard to explain, see last conditional in paper loop
          let dx = fromRational <$> (x pointNew - x lastBFGS)
              dgrad = (gradient pointNew - gradient lastBFGS)
              curvatureCondition = dot dx dgrad > 0
              pointNew' = pointNew { hessianGuess =
                                       if curvatureCondition
                                       then updatedHessian lastBFGS pointNew
                                       else hessianGuess point
                                   }
              lastBFGS' = if curvatureCondition then pointNew' else lastBFGS
          -- done, might've updated hessian
          go (iter+1) (pointNew' : pointList) lastBFGS'
    -- quash warning (should never call this with empty list of points)
    go _ [] _ = undefined

-- | Like extremizeIsland, but works for non-convex navigator function
extremizeIslandSafe
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => IslandExtConfig n
  -> (Rational -> V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> m (V n Rational, [BFGSData n a])
extremizeIslandSafe MkIslandExtConfig{..} getValueAndGradient initialPointData direction =
  go 1 [initialPointData] stepDir0
  where
    stepRes = stepResolution bfgsConfig
    -- aggressiveness of pivot
    beta = 0.8
    -- testing of end condition
    endCondition constant gradient = let
      gtol = fromRational goalToleranceObj
      gtolD = fromRational goalToleranceGrad
      projected = L.project (fromRational <$> direction) gradient
      in (abs constant <= gtol) && (norm (gradient - projected) <= gtolD)
    -- initial step size:
    -- if estimate from grad places us outside bounding box, go halfway to boundary
    dirSize = norm (fromRational <$> direction)
    gradRatio = abs (constant initialPointData) / norm (gradient initialPointData)
    wallDist = boxWallDistance bfgsConfig (x initialPointData) direction
    -- stay within bounding box
    -- can't use `min` because rationals have no Infinity
    stepDir0 = if fromRational wallDist < (gradRatio / dirSize)
               then (wallDist / 2) *^ direction
               else approxRational' stepRes (gradRatio / dirSize) *^ direction
    -- Main flow control
    go iter pointList@(point:_) stepDirection
      | fmap (<iter) maxIters == Just True             = pure (x point, pointList)
      | endCondition (constant point) (gradient point) = pure (x point, pointList)
      | otherwise = do
          -- search in direction of given line
          -- if already below the threshold, let |N| increase a little bit
          let goalRat = goalToleranceObj / approxRational' stepRes (abs (constant point))
          pointNew <- bisectingLineSearch
                      (getValueAndGradient (epsGradHess bfgsConfig))
                      point stepDirection stepRes
                      (max lineSearchDecrease goalRat)
          -- pivot to new search direction
          let stepSize = norm (fromRational <$> (x pointNew - x point))
              aHat = signorm (fromRational <$> direction)
              gHat = signorm (gradient pointNew)
              stepDirNext = signorm $
                (beta / dot aHat gHat + (1-beta) * dot aHat gHat) *^ aHat - gHat
              stepNext = approxRational' stepRes <$> (stepSize *^ stepDirNext)
          go (iter + 1) (pointNew : pointList) stepNext
    -- quash warning (should never call this with empty list of points)
    go _ [] _ = undefined

-- | Core updating math of BFGS
bfgsStep
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => Config n
  -> (Rational -> V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> m (BFGSData n a)
bfgsStep cfg@MkConfig{..} getValueAndGradient point@MkBFGSData{..} = do
  pointNew <- approxLineSearch (getValueAndGradient epsGradHess)
              point' stepDirection
              (boxWallDistance cfg x stepDirection)
  -- line search probably won't provide hessian, need to get it post-hoc
  pure $ pointNew{ hessianGuess = updatedHessian point' pointNew }
  where
    point' = withHessianGuess cfg point
    stepDirection  = approxNewtonStep cfg point'

-- | Distance to the next box wall
boxWallDistance
  :: KnownNat n
  => Config n -> V n Rational -> V n Rational -> Rational
boxWallDistance MkConfig{..} x stepDirection = minimum allDistances
  where
    dists1 = toList (boundingBoxMax - x)
    dists2 = toList (boundingBoxMin - x)
    stepComponents = toList stepDirection
    -- get positive scaled distance along component to wall (unless 0)
    mbMaxDist _ _ 0 = Nothing
    mbMaxDist u l s = Just $ max (u / s) (l / s)
    allDistances = catMaybes $ zipWith3 mbMaxDist dists1 dists2 stepComponents

-- | Given the BFGSData for a point, return a new BFGSData with our best guess
-- for the Hessian
withHessianGuess :: (KnownNat n, RealFrac a, Floating a)
                 => Config n -> BFGSData n a -> BFGSData n a
withHessianGuess MkConfig{..} point =
  point{ hessianGuess = finalHessian }
  where
    -- if user doesn't provide a default hessian, use bounding box
    boxDims = fromRational <$> (boundingBoxMax - boundingBoxMin)
    gradientScale = norm (gradient point) / 0.2 -- from Navigator paper alg. 1
    -- try provided hessian, then init guess in config, then bbox as last resort
    finalHessian = hessianGuess point
                   <|> ((fromRational <$>) <$> initialHessianGuess)
                   <|> Just (L.diagonalMatrix $ (gradientScale /) <$> boxDims)

-- | Given the BFGSData for a point, return a rational approximation to the next
-- step in a Newton search
approxNewtonStep :: (KnownNat n, RealFrac a, Floating a)
                 => Config n -> BFGSData n a -> V n Rational
approxNewtonStep cfg@MkConfig{..} point =
  approxRational' stepResolution <$> stepDir
  where
    point' = withHessianGuess cfg point
    stepDir = Jet2.newtonStep $ Jet2.MkJet2
              (constant point')
              (gradient point')
              (fromJust $ hessianGuess point')

-- | Given two points, maybe return an approximation to the Hessian of the
-- second based on the Hessian of the first.
--
-- Returns Just the Hessian guess of point2 if it exists, returns Nothing if
-- neither point has Hessian information, otherwise returns the result of the
-- BFGS formula.
updatedHessian
  :: (KnownNat n, RealFrac a)
  => BFGSData n a
  -> BFGSData n a
  -> Maybe (Matrix n n a)
updatedHessian point1 point2 =
  case hessianGuess point2 of
    Just bfinal -> Just bfinal
    Nothing -> do
      b0 <- (hessianGuess point1) -- fail if point1 is Nothing
      -- step size and change in gradient (same notation as Wikipedia):
      let s = fromRational <$> (x point2 - x point1)
          y = gradient point2 - gradient point1
          b0s = b0 L..* s
          bDiff = (L.outer y y) ^/ (dot s y)
                  - (L.outer b0s b0s) ^/ (dot s b0s)
      pure (b0 + bDiff)

-- | Given a second-order approximation to a function N(x), find the extremum
-- (in the given direction) of the ellipsoid where N(x) = 0
extremizeEllipsoid
  :: (KnownNat n, RealFrac a, Floating a)
  => BFGSData n a
  -> V n a
  -> V n a
extremizeEllipsoid MkBFGSData{..} direction =
  -- (-lambda) will give another extremum, but this direction because we
  -- assume the hessian is positive-definite
  x' - (applyInverseB gradient) + (applyInverseB direction) ^/ lambda
  where
    x' = fromRational <$> x
    hessian = fromJust hessianGuess -- we never call this without a Hessian
    applyInverseB = L.luSolve (L.toRows hessian)
    lambda = sqrt ( (direction `dot` (applyInverseB direction))
                    / (gradient `dot` (applyInverseB gradient) - 2*constant)
                  )

-- | Monadic implementation of an approximate line search satisfying the Wolfe
-- conditions, from Wright & Nocedal p. 60
-- (I think it's the same as More-Thuente, but the M-T paper is unreadable)
-- Returns grad information too, so we don't have to redo a job
approxLineSearch
  :: (KnownNat n, RealFrac a, Monad m)
  => (V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> Rational
  -> m (BFGSData n a)
approxLineSearch getBFGSData initialPoint direction alphaMax =
  if alphaMax > 0
  then go (1::Int) alpha0 0.0 (constant initialPoint)
  else pure (initialPoint {searchStatus = BoundaryExceeded})

  where
    mu    = 0.0001 -- for sufficient decrease condution
    eta   = 0.9    -- for curvature condition
    delta = 1.5    -- factor to multiply alpha by when we extend our region

    -- setup for solver call
    alpha0 = if alphaMax > 1 then 1 else alphaMax/2
    (f0, df0) = bfgsDataToJet1 initialPoint

    go iter alpha alphaPrev fPrev = do
      pointData <- getBFGSData ((x initialPoint) + alpha *^ direction)
      let (f, df) = bfgsDataToJet1 pointData
      if | alpha > alphaMax         -> pure (pointData {searchStatus = BoundaryExceeded})
         | (f > f0 + df0*mu*(fromRational alpha)) || (iter > 1 && f >= fPrev)
                                    -> zoom alphaPrev alpha fPrev
         | abs df <= -eta * df0     -> pure pointData
         | df > 0                   -> zoom alpha alphaPrev f
         | otherwise                -> go (iter+1) (delta * alpha) alpha f

    zoom low high fLow = do -- (low < high) is not necessarily true
      let alpha = interpolate low high
      pointData <- getBFGSData ((x initialPoint) + alpha *^ direction)
      let (f, df) = bfgsDataToJet1 pointData
      if | (f > f0 + df0*mu*(fromRational alpha)) || (f >= fLow)
                                                   -> zoom low alpha fLow
         | abs df <= -eta * df0                    -> pure pointData
         | df * (fromRational (high - low)) >= 0   -> zoom alpha low f
         | otherwise                               -> zoom alpha high f

    bfgsDataToJet1 dat = (constant dat,
                          dot (gradient dat) (fromRational <$> direction))

    interpolate low high = (low + high) / 2.0 -- could use other methods

--- stuff
bisectingLineSearch
  :: (KnownNat n, RealFrac a, Monad m)
  => (V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> Rational
  -> Rational
  -> m (BFGSData n a)
bisectingLineSearch getBFGSData initialPoint direction
  stepResolution decreaseThreshold =
  do
    (alpha1, point1, alpha2, point2) <- findSignChange 0 initialPoint alpha0
    if constant point2 / f0 > 0
      then pure point2
      else bisect alpha1 point1 alpha2 point2
  where
    alpha0 = 1.0
    alphaScale = 2.0
    f0 = constant initialPoint
    thresh = fromRational decreaseThreshold

    -- get first point of either opposite sign or below threshold
    -- also returns last point that did NOT satisfy this condition
    findSignChange alphaPrev pointPrev alpha = do
      point <- getBFGSData ((x initialPoint) + alpha *^ direction)
      if constant point / f0 <= thresh
        then pure (alphaPrev, pointPrev, alpha, point)
        else findSignChange alpha point (alphaScale * alpha)

    -- given two points of opposite sign, find a point on the left of the root
    -- and below the threshold
    bisect alpha1 point1 alpha2 point2 = do
      let y1 = approxRational' stepResolution $ constant point1 / f0
          y2 = approxRational' stepResolution $ constant point2 / f0
          -- clamp in case one is ill-conditioned
          intervalFrac = max 0.5 . min 0.5 $ (y1 - decreaseThreshold / 2) / (y1 - y2)
          alpha' = alpha1 + (alpha2 - alpha1) * intervalFrac
      point' <- getBFGSData ((x initialPoint) + alpha' *^ direction)
      let y' = constant point' / f0
      if | y' > thresh -> bisect alpha' point' alpha2 point2
         | y' < 0      -> bisect alpha1 point1 alpha' point'
         | otherwise   -> pure point'

interpolatingLineSearch
  :: (KnownNat n, RealFrac a, Floating a, Monad m)
  => (V n Rational -> m (BFGSData n a))
  -> BFGSData n a
  -> V n Rational
  -> Rational
  -> Rational
  -> m (BFGSData n a)
interpolatingLineSearch getBFGSData initialPoint direction
  stepResolution decreaseThreshold = go alpha0
  where
    navTarget = (fromRational decreaseThreshold) * abs (constant initialPoint)
    alpha0 = 1.0

    go alpha = do
      alphaPoint <- getBFGSData ((x initialPoint) + alpha *^ direction)
      let (f0, df0) = bfgsDataToJet1 initialPoint
          (f1, df1) = bfgsDataToJet1 alphaPoint
          interpolator = cubicInterpolatorUnit f0 df0 f1 df1
      if abs (constant alphaPoint) <= navTarget -- CHANGE
        then pure alphaPoint
        else if constant initialPoint < 0
             then go $ alpha * approxRational' stepResolution (solveCubic' interpolator)
             else go $ alpha * approxRational' stepResolution (extremizeCubic' interpolator)

    -- gets all roots, returns the one closest to 0.5 (middle of test interval)
    solveCubic' cubic = foldr1 selector (solveCubic cubic)
      where selector x y = if abs (x-0.5) < abs (y-0.5) then x else y

    -- gets minimum if it exists, otherwise return default of 0.5
    extremizeCubic' cubic = case (extremizeCubic cubic) of
                              []      -> 0.5
                              extrema -> foldr1 selector extrema
      where selector = if (c3 cubic > 0) then max else min

    bfgsDataToJet1 dat = (constant dat,
                          dot (gradient dat) (fromRational <$> direction))

data CubicPolynomial a = MkCubic
  { c0 :: a,
    c1 :: a,
    c2 :: a,
    c3 :: a
  } deriving (Eq, Ord, Show)

---- | Evaluates a cubic polynomial at the given value of x
--evalCubic
--  :: (RealFrac a)
--  => CubicPolynomial a -> a -> a
--evalCubic MkCubic{..} x = c0 + c1*x + c2*x*x + c3*x*x*x

-- | Constructs a cubic interpolating polynomial on the unit interval
-- given the values and derivatives of a function at 0 and 1
cubicInterpolatorUnit
  :: (RealFrac a)
  => a -> a -> a -> a -> CubicPolynomial a
cubicInterpolatorUnit f0 df0 f1 df1 = MkCubic{..}
  where -- from gaussian elimination:
    c0 = f0
    c1 = df0
    c2 = 3*(f1 - c0 - c1) - (df1 - c1)
    c3 = f1 - c0 - c1 - c2

-- | Given a cubic equation, returns a list of all real local extrema
extremizeCubic
  :: (RealFrac a, Floating a)
  => CubicPolynomial a -> [a]
extremizeCubic MkCubic{..} = realQuadraticRoots c1 (2*c2) (3*c3)

-- | Given a cubic, returns a list of all real roots
--   roots with multiplicity >1 are returned multiple times
solveCubic
  :: (RealFrac a, Floating a)
  => CubicPolynomial a -> [a]
solveCubic MkCubic{..}
  | c3 == 0            = realQuadraticRoots c0 c1 c2 -- just a quadratic
  | p == 0 && q == 0   = unDepress <$> [0, 0, 0]     -- triple root
  | p == 0             = unDepress <$> [cubeRoot (-q)] -- single root, no x term
  | discriminant <  0  = unDepress <$> depressedSingleRoot -- general single root
  | otherwise          = unDepress <$> depressedMultiRoots -- general 2-3 roots
  where
    discriminant = 18*c3*c2*c1*c0 - 4*c2*c2*c2*c0 + c2*c2*c1*c1
                   -4*c3*c1*c1*c1 - 27*c3*c3*c0*c0 -- to get number of roots

    -- do Cardano reduction and solve depressed cubic with trig:
    depressedMultiRoots = [ 2 * sqrt(-p/3)
                            * cos( acos(trigClamp tmp)/3 - 2*pi*k/3 )
                          | k <- [0,1,2] ]
    depressedSingleRoot = if p >= 0 then
                            [ -2 * sqrt(p/3) * sinh((asinh tmp) / 3) ]
                          else
                            [ -2 * (signum q) * sqrt(-p/3)
                              * cosh((acosh (abs tmp)) / 3) ]
    tmp = sqrt(3/(abs p)) * (3*q) / (2*p)

    -- definition of depressed cubic:
    p = (3*c3*c1 - c2*c2) / (3*c3*c3)
    q = (2*c2*c2*c2 - 9*c3*c2*c1 + 27*c3*c3*c0) / (27*c3*c3*c3)
    unDepress t = t - c2 / (3 * c3)

    -- fix overflow before calling asin or acos
    trigClamp x | x >  1    =  1
                | x < -1    = -1
                | otherwise = x

-- | utility functions for intermediate steps in cubic polynomials
realQuadraticRoots :: (RealFrac a, Floating a) => a -> a -> a -> [a]
realQuadraticRoots c0 c1 c2
  | c2 == 0            = [-c0 / c1] -- no x^2 term; just a line
  | discriminant  < 0  = []
  | discriminant == 0  = [-c1 / (2*c2)]
  | otherwise          = [ (-c1 - sqrt discriminant) / (2*c2)
                         , (-c1 + sqrt discriminant) / (2*c2)
                         ]
  where
    discriminant = c1*c1 - 4*c0*c2

cubeRoot :: (RealFrac a, Floating a) => a -> a
cubeRoot x | x < 0     = -cubeRoot (-x)
           | otherwise = x**(1/3)
