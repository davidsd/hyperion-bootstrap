{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TypeFamilies      #-}

module Hyperion.Bootstrap.SDPDeriv.Bound where

import Bootstrap.Math.AffineTransform          (AffineTransform (..))
import Bootstrap.Math.Linear                   qualified as L
import Bootstrap.Math.VectorSpace              ((^/))
import Control.Distributed.Process             (unClosure)
import Control.Monad                           (forM, void, when)
import Control.Monad.IO.Class                  (liftIO)
import Control.Monad.State                     (StateT, evalStateT, get, lift,
                                                modify)
import Data.Aeson                              (ToJSON)
import Data.Binary                             (Binary)
import Data.BinaryHash                         (hashBase64Safe)
import Data.Maybe                              (fromMaybe)
import Data.Text                               (Text)
import GHC.Generics                            (Generic)
import GHC.TypeNats                            (KnownNat, Nat)
import Hyperion                                (Cluster, Job, newWorkDir,
                                                remoteEvalJob)
import Hyperion.Bootstrap.Bound                (Bound (..),
                                                BoundFileTreatment (..),
                                                BoundFiles,
                                                CanBuildSDP, FileTreatment (..),
                                                SDPFetchBuildConfig, ToSDP,
                                                checkpointDir,
                                                cleanFilesWithTreatment,
                                                computeWithFileTreatment,
                                                defaultBoundFiles,
                                                initialCheckpointDir,
                                                makeSDPDir, outDir, scriptsDir,
                                                sdpDir)
import Hyperion.Bootstrap.OPESearch            (TrackedMap)
import Hyperion.Bootstrap.OPESearch            qualified as OPE
import Hyperion.Bootstrap.SDPDeriv.BFGS        qualified as BFGS
import Hyperion.Bootstrap.SDPDeriv.Jet2        qualified as Jet2
import Hyperion.Bootstrap.SDPDeriv.Newton      qualified as Newton
import Hyperion.Bootstrap.SDPDeriv.NewtonTrust qualified as NewtonTrust
import Hyperion.Bootstrap.SDPDeriv.Run         qualified as SDPDeriv
import Hyperion.Concurrent                     (forConcurrently)
import Hyperion.Database                       qualified as DB
import Hyperion.Log                            qualified as Log
import Hyperion.Static                         (Closure, Dict (..), Static (..),
                                                cAp, cPtr, cPure)
import Hyperion.Static.Orphans                 ()
import Linear.Matrix                           (identity)
import Linear.V                                (V)
import Numeric.Rounded                         (Rounded, RoundingMode (..))
import SDPB qualified
import System.Directory                        (removePathForcibly)
import System.FilePath.Posix                   (replaceFileName, (<.>), (</>))
import Type.Reflection                         (Typeable)

type BigFloat (p :: Nat) = Rounded 'TowardZero p

-- aliases for shorter declarations
type MyDict n p b =
  Dict ( KnownNat n
       , KnownNat p
       , Show b
       , Typeable b
       , ToJSON b
       , Static (ToSDP b)
       , Static (Binary b)
       , Static (SDPFetchBuildConfig b)
       )

type RemoteBoundConstraints b =
  ( Typeable b
  , Static (Binary b), Static (Show b), Static (ToSDP b)
  , Static (ToJSON b), Static (SDPFetchBuildConfig b))

mapBoundParams :: (SDPB.Params -> SDPB.Params) -> Bound a b -> Bound a b
mapBoundParams f b = b { solverParams = f (solverParams b) }

setCentralPathParams :: Int -> SDPB.Params -> SDPB.Params
setCentralPathParams iterations params = params
  { SDPB.maxIterations                = iterations
  , SDPB.stepLengthReduction          = 1
  , SDPB.infeasibleCenteringParameter = 1
  , SDPB.dualityGapThreshold          = 0
  , SDPB.primalErrorThreshold         = 0
  , SDPB.dualErrorThreshold           = 0
  }

setWriteFullSolution :: SDPB.Params -> SDPB.Params
setWriteFullSolution params = params { SDPB.writeSolution = SDPB.allSolutionParts }

-- | Computes a bound and performs additional "centering iterations"
-- toward the central path.
computeWithCentering
  :: CanBuildSDP b
  => Int
  -> BoundFileTreatment
  -> Bound Int b
  -> BoundFiles
  -> Job (SDPB.Output)
computeWithCentering centerings treatment bound files
  | centerings < 1 =
    computeWithFileTreatment treatment (mapBoundParams setWriteFullSolution bound) files
  | otherwise = do
      -- We must be sure to keep the sdp and checkpoint dirs so that
      -- the centering run below works correctly.
      let keepSdpCk = treatment
            { sdpDirTreatment        = KeepFile
            , checkpointDirTreatment = KeepFile
            }
      resultNormal <- computeWithFileTreatment keepSdpCk bound files
      let
        boundWithCentering = mapBoundParams
          (setWriteFullSolution . setCentralPathParams centerings)
          bound
        -- don't use DB checkpoint for centering runs
        files' = files {initialCheckpointDir = Nothing}
      -- only do centering if initial run terminated correctly
      resultCentering <- if SDPB.terminateReason resultNormal == SDPB.PrimalDualOptimal
                         then computeWithFileTreatment treatment boundWithCentering files'
                         else (pure resultNormal)
      -- Centering iterations always terminate with
      -- MaxIterationsExceeded, even if the problem is solved to
      -- optimality. So we set the terminateReason back to the one
      -- from the normal run of SDPB.
      pure $ resultCentering { SDPB.terminateReason = SDPB.terminateReason resultNormal }

-- | Assumes that bound1 has already been solved and that the solution
-- (x,y,X,Y) exists on disk. Looks for approx objective at
-- 'scriptsDir/srun_approx_objective.sh', where 'scriptsDir' is set by
-- 'BoundConfig'.
sdpDerivBoundPair
  :: (CanBuildSDP b, Fractional a)
  => Bound Int b
  -> BoundFiles
  -> Bound Int b
  -> BoundFiles
  -> Job (SDPDeriv.Output a)
sdpDerivBoundPair bound1 files1 bound2 files2 = do
  void $ makeSDPDir bound2 files2
  let
    prec = SDPB.precision (solverParams bound1)
    input = SDPDeriv.MkInput
      { SDPDeriv.sdpDerivativeMode   = SDPDeriv.SDP2_B_b_c
      , SDPDeriv.sdp1Dir             = sdpDir files1
      , SDPDeriv.sdp1OutDir          = outDir files1
      , SDPDeriv.sdp2Dir             = sdpDir files2
      , SDPDeriv.precision           = prec
      , SDPDeriv.approxObjExecutable = scriptsDir (boundConfig bound1) </> "srun_approx_objective.sh"
      }
  liftIO $ SDPDeriv.sdpDerivative input

-- | Compute 'bound0' and run 'sdp_derivative' on 'bounds'. This
-- function shares block and json directories between all of the
-- bounds, so that blocks and json files can be used repeatedly when
-- appropriate. (Checkpoint and out directories are only needed for
-- bound0.) The appropriate directories are deleted afterwards
-- according to 'treatment'.
sdpDerivBoundsSharedFiles
  :: (Fractional a, CanBuildSDP b, Traversable t)
  => Int
  -> BoundFileTreatment
  -> Bound Int b
  -> t (Bound Int b)
  -> BoundFiles
  -> Job (SDPB.Output, t (SDPDeriv.Output a))
sdpDerivBoundsSharedFiles centerings treatment bound0 bounds files0 = do
  let
    -- For the initial computation of bound0, we keep the sdp and out
    -- directory for use with sdp_derivative. We keep the json and
    -- block directories so they can be shared with the other 'bounds'
    -- when possible.
    keepFiles = treatment
      { jsonDirTreatment  = KeepFile
      , sdpDirTreatment   = KeepFile
      , outDirTreatment   = KeepFile
      , blockDirTreatment = KeepFile
      }
  result0 <- computeWithCentering centerings keepFiles bound0 files0
  variations <- forM bounds $ \bound1 -> do
    let files1 = files0 { sdpDir = replaceFileName (sdpDir files0)
                                   (hashBase64Safe bound1 <.> "sdp")
                        }
    variation <- sdpDerivBoundPair bound0 files0 bound1 files1
    -- After running sdp_derivative, the sdp file for bound1 is not
    -- needed anymore, so we can remove it if 'treatment' allows.
    when (sdpDirTreatment treatment == RemoveFile) $ liftIO $
      removePathForcibly (sdpDir files1)
    pure variation
  cleanFilesWithTreatment treatment files0
  pure (result0, variations)

-- | Compute (mkBound 0) and the gradient of the objective at 0.
computeAndGradient
  :: (KnownNat n, Fractional a, CanBuildSDP b)
  => Int
  -> BoundFileTreatment
  -> BoundFiles
  -> (V n Rational -> Bound Int b)
  -> Rational
  -> Job (SDPB.Output, V n a)
computeAndGradient centerings treatment files mkBound eps = do
  let
    bound0 = mkBound 0
    bounds = mkBound . fmap (*eps) <$> identity
  (result0, variations) <- sdpDerivBoundsSharedFiles centerings treatment bound0 bounds files
  let  mkGradComponent var = SDPDeriv.linear var / fromRational eps
  pure (result0, fmap mkGradComponent variations)

-- | Compute (mkBound 0) and a Jet2 representing the quadratic
-- approximation to the objective around zero.
computeAndJet2
  :: (KnownNat n, Fractional a, CanBuildSDP b)
  => Int
  -> BoundFileTreatment
  -> BoundFiles
  -> (V n Rational -> Bound Int b)
  -> Rational
  -> Job (SDPB.Output, Jet2.Jet2 n a)
computeAndJet2 centerings treatment files mkBound eps = do
  let
    bound0 = mkBound 0
    bounds = fmap mkBound (Jet2.tautologyPoints eps)
  (result0, variations) <- sdpDerivBoundsSharedFiles centerings treatment bound0 bounds files
  pure ( result0
       , Jet2.fromPoints
         (realToFrac (SDPB.dualObjective result0))
         (fmap SDPDeriv.total variations)
       )

data GetBoundJetConfig n b = GetBoundJetConfig
  { centeringIterations :: Int
  , fileTreatment       :: BoundFileTreatment
  , boundClosure        :: Closure (V n Rational -> Bound Int b)
  , valFromObjClosure   :: Closure Jet2.FractionalMap
  , initialPoint        :: V n Rational
  , checkpointMapName   :: Maybe Text
  } deriving (Eq, Ord, Show, Generic, Binary)

instance (KnownNat n, Typeable b) => Static (Binary (GetBoundJetConfig n b)) where
  closureDict = cPtr (static (\Dict -> Dict)) `cAp` closureDict @(KnownNat n)

getBoundJet
  :: MyDict n p b
  -> GetBoundJetConfig n b
  -> Job (Rational -> V n Rational -> StateT Int Job (Jet2.Jet2 n (BigFloat p)))
getBoundJet Dict cfg = do
  mkBound <- lift $ unClosure (boundClosure cfg)
  valFromObjective <- lift $ unClosure (valFromObjClosure cfg)
  let boundStart = mkBound (initialPoint cfg)
  workDir <- newWorkDir boundStart
  pure $ \eps x -> do
    nStep <- get
    let
      bound = mkBound x
      slug = "step_" <> show nStep <> "_" <> hashBase64Safe bound
      files = defaultBoundFiles (workDir </> slug)
    (sdpbOutput, j') <- lift $ computeAndJet2 (centeringIterations cfg) (fileTreatment cfg) files (mkBound . (+x)) eps
    if SDPB.terminateReason sdpbOutput /= SDPB.PrimalDualOptimal
      then Log.throwError "SDPB could not find primal-dual optimal point"
      else do
      let j = Jet2.compose valFromObjective j'
      Log.info "Computed Jet2 for bound" (bound, nStep, sdpbOutput, x, j)
      DB.insert (DB.KeyValMap "jets") (boundStart, nStep) (bound, sdpbOutput, x, j)
      modify (+1)
      pure j

getBoundBFGSData
  :: MyDict n p b
  -> GetBoundJetConfig n b
  -> TrackedMap Job (V n Rational) FilePath
  -> Job (Rational -> V n Rational -> StateT Int Job (BFGS.BFGSData n (BigFloat p)))
getBoundBFGSData Dict cfg checkpointMap = do
  mkBound <- lift $ unClosure (boundClosure cfg)
  valFromObjective <- lift $ unClosure (valFromObjClosure cfg)
  let boundStart = mkBound (initialPoint cfg)
  workDir <- newWorkDir boundStart
  pure $ \eps x -> do
    nStep <- get
    mCheckpoint <- lift $ OPE.get checkpointMap x

    let
      bound = mkBound x
      -- track checkpoint directories
      slug = "step_" <> show nStep <> "_" <> hashBase64Safe bound
      filesWithoutCheckpoint = defaultBoundFiles (workDir </> slug)
      files = filesWithoutCheckpoint {
        -- don't use non-existent checkpoints
        initialCheckpointDir = if mCheckpoint == Just ""
                               then Nothing
                               else mCheckpoint
        }
      -- helper functions
      computeBG f = computeAndGradient (centeringIterations cfg)
                    (fileTreatment cfg) f (mkBound . (+x)) eps
      compSucceeded sOut = SDPB.terminateReason sOut == SDPB.PrimalDualOptimal
    -- before running, write empty checkpoint map so we know to avoid on a retry
    -- (TODO: refactor, this is a hack)
    lift $ OPE.set checkpointMap x ""
    -- COMPUTATION RUNS HERE:
    (sdpbOutput, grad) <- lift $ computeBG filesWithoutCheckpoint
    -- (sdpbOutput, grad) <- (lift $ computeBG files) >>= (
    --   \(sOut, gr) -> if compSucceeded sOut then pure (sOut, gr) else do
    --     liftIO $ removePathForcibly (checkpointDir files)
    --     lift $ computeBG filesWithoutCheckpoint
    --   )
    if not $ compSucceeded sdpbOutput
      then Log.throwError "SDPB could not find primal-dual optimal point"
      else do
      if (mCheckpoint == Just "" || mCheckpoint == Nothing)
        then lift $ OPE.set checkpointMap x (checkpointDir files)
        else pure ()
      let
        -- hess' is irrelevant since we throw out hessian info, but we need
        -- something of the correct type to use Jet2.Compose
        hess' = L.outer grad grad
        -- jet2 of pure navigator N(x)
        j' = Jet2.MkJet2 (realToFrac (SDPB.dualObjective sdpbOutput)) grad hess'
        -- jet2 of f(x)
        j = Jet2.compose valFromObjective j'
        bfgsData = BFGS.MkBFGSData x (Jet2.constant j) (Jet2.gradient j) Nothing BFGS.Success
      Log.info "Computed BFGS jet for bound" (bound, bfgsData)
      DB.insert (DB.KeyValMap "bfgsJets") (boundStart, nStep) (bound, sdpbOutput, bfgsData)
      modify (+1)
      pure bfgsData

newtonBound
  :: MyDict n p b
  -> Newton.Config
  -> GetBoundJetConfig n b
  -> Job (V n Rational, [Jet2.Jet2 n (BigFloat p)])
newtonBound dict@Dict nsCfg jbCfg = do
  getJet <- getBoundJet dict jbCfg
  evalStateT (Newton.run nsCfg getJet (initialPoint jbCfg)) 1

remoteNewtonBound
  :: ( KnownNat n , KnownNat p , RemoteBoundConstraints b )
  => Newton.Config
  -> GetBoundJetConfig n b
  -> Cluster (V n Rational, [Jet2.Jet2 n (BigFloat p)])
remoteNewtonBound nsCfg jbCfg = do
  remoteEvalJob $
    cPtr (static newtonBound) `cAp` closureDict `cAp` cPure nsCfg `cAp` cPure jbCfg

newtonTrustBound
  :: MyDict n p b
  -> NewtonTrust.Config n p
  -> GetBoundJetConfig n b
  -> Job (V n Rational, [Jet2.Jet2 n (BigFloat p)])
newtonTrustBound dict@Dict nsCfg jbCfg = do
  getJet <- getBoundJet dict jbCfg
  evalStateT (NewtonTrust.run nsCfg getJet (initialPoint jbCfg)) 1

remoteNewtonTrustBound
  :: ( KnownNat n , KnownNat p , RemoteBoundConstraints b )
  => NewtonTrust.Config n p
  -> GetBoundJetConfig n b
  -> Cluster (V n Rational, [Jet2.Jet2 n (BigFloat p)])
remoteNewtonTrustBound nsCfg jbCfg = do
  remoteEvalJob $
    cPtr (static newtonTrustBound) `cAp` closureDict `cAp` cPure nsCfg `cAp` cPure jbCfg

bfgsBound
  :: MyDict n p b
  -> BFGS.Config n
  -> GetBoundJetConfig n b
  -> Job (V n Rational, [BFGS.BFGSData n (BigFloat p)])
bfgsBound dict@Dict bCfg jbCfg = do
  let mapName = fromMaybe "bfgsCheckpoints" (checkpointMapName jbCfg)
  cpointMap <- newBoundingBoxCheckpointMap mapName
               (BFGS.boundingBoxMin bCfg) (BFGS.boundingBoxMax bCfg)
               Nothing
  getJet <- getBoundBFGSData dict jbCfg cpointMap
  evalStateT (BFGS.run bCfg getJet (initialPoint jbCfg)) 1

evalSingleGrad
  :: MyDict n p b
  -> GetBoundJetConfig n b
  -> Rational
  -> V n Rational
  -> Job (BFGS.BFGSData n (BigFloat p))
evalSingleGrad dict@Dict jbCfg epsGrad point = do
  let blankMap = OPE.constTrackedMap Nothing
  getJet <- getBoundBFGSData dict jbCfg blankMap
  evalStateT (getJet epsGrad point) 1

-- | Given the navigator data for a feasible point, find the extremal point of
-- the feasible region in the given direction
bfgsExtremizeDirection
  :: MyDict n p b
  -> BFGS.IslandExtConfig n
  -> GetBoundJetConfig n b
  -> BFGS.BFGSData n (BigFloat p)
  -> V n Rational
  -> Job (V n Rational, [BFGS.BFGSData n (BigFloat p)])
bfgsExtremizeDirection dict@Dict ixCfg jbCfg initData direction = do
  Log.info "starting bfgsExtremizeDirection with init data" initData
  Log.info "direction is" direction
  let bCfg = BFGS.bfgsConfig ixCfg
      mapName = fromMaybe "bfgsCheckpoints" (checkpointMapName jbCfg)
  cpointMap <- newBoundingBoxCheckpointMap mapName
               (BFGS.boundingBoxMin bCfg) (BFGS.boundingBoxMax bCfg)
               Nothing
  getJet <- getBoundBFGSData dict jbCfg cpointMap
  Log.info "going into run with config" ixCfg
  evalStateT (BFGS.extremizeIslandSafe ixCfg getJet initData direction) 1

remoteBFGSBound
  :: ( KnownNat n , KnownNat p , RemoteBoundConstraints b )
  => BFGS.Config n
  -> GetBoundJetConfig n b
  -> Cluster (V n Rational, [BFGS.BFGSData n (BigFloat p)])
remoteBFGSBound bCfg jbCfg = do
  remoteEvalJob $
    cPtr (static bfgsBound) `cAp` closureDict `cAp`
    cPure bCfg `cAp` cPure jbCfg

-- | Descends to an island and finds the extrema (along the specified directions)
-- of the allowed regions. Returns an array of the extremal points
remoteBFGSExtremize
  :: ( KnownNat n , KnownNat p , RemoteBoundConstraints b )
  => BFGS.IslandExtConfig n
  -> GetBoundJetConfig n b
  -> [V n Rational]
  -> Cluster ([V n Rational], BFGS.BFGSData n (BigFloat p))
remoteBFGSExtremize ixCfg jbCfg directions = do
  (_, allPoints) <- remoteEvalJob $
                    cPtr (static bfgsBound) `cAp` closureDict `cAp`
                    cPure (BFGS.bfgsConfig ixCfg) `cAp` cPure jbCfg
  let lastPoint = case allPoints of
        p : _ -> p
        []    -> error "Unexpected allPoints = []"
  jobResults <- forConcurrently directions (
    \dir -> remoteEvalJob $
            cPtr (static bfgsExtremizeDirection) `cAp` closureDict `cAp`
            cPure ixCfg `cAp` cPure jbCfg `cAp`
            cPure lastPoint `cAp` cPure dir
    )
  pure (fmap fst jobResults, lastPoint)

-- | Like remoteBFGSExtremize, but use several samples to guess hessian
remoteBFGSExtremize'
  :: ( KnownNat n , KnownNat p , RemoteBoundConstraints b )
  => BFGS.IslandExtConfig n
  -> GetBoundJetConfig n b
  -> [V n Rational]
  -> Cluster ([V n Rational], BFGS.BFGSData n (BigFloat p))
remoteBFGSExtremize' ixCfg jbCfg directions = do
  hess' <- remoteSampleHessian jbCfg
  let bCfg = BFGS.bfgsConfig ixCfg
      bCfg' = bCfg{BFGS.initialHessianGuess = Just hess'}
  (xsTmp, dataTmp) <- remoteBFGSExtremize ixCfg{BFGS.bfgsConfig = bCfg'}
                      jbCfg
                      directions
  --Log.info "ignore this" (head initData0 == dataTmp)
  pure (xsTmp, dataTmp)

-- | use several samples to guess hessian directly
remoteSampleHessian
  :: ( KnownNat n , RemoteBoundConstraints b )
  => GetBoundJetConfig n b
  -> Cluster (L.Matrix n n Rational)
remoteSampleHessian bjCfg = do
  -- get gradients at all points
  let dx = 0.0001 :: Rational
      x0 = initialPoint bjCfg
      deltas = L.toRows $ L.diagonalMatrix (dx <$ x0)
      trialPts = (+x0) <$> deltas
  initData0 :: [BFGS.BFGSData n (Rounded 'TowardZero 200)] <-
    remoteEvalGrads bjCfg [x0]
  initData :: V n (BFGS.BFGSData n (Rounded 'TowardZero 200)) <-
    remoteEvalGrads bjCfg trialPts
  -- calculate trial hessian
  let approxRat = Newton.approxRational' (1e-32)
      -- differencing:
      grad0 = case initData0 of
        i : _ -> BFGS.gradient i
        []    -> error "Unxpected initData0 = []"
      dgrads = (\dat -> (approxRat <$> (BFGS.gradient dat - grad0)) ^/ dx)
               <$> initData
      hess' = (\x y -> (x + y) / 2) <$> L.fromRows dgrads <*> L.fromCols dgrads
  Log.info "test hessian" $ fmap (fromRational :: Rational -> Double) hess'
  return hess'

-- | Evaluates the gradient of a navigator function at multiple points, mostly for testing
remoteEvalGrads
  :: ( KnownNat n , KnownNat p , Traversable t , RemoteBoundConstraints b )
  => GetBoundJetConfig n b
  -> t (V n Rational)
  -> Cluster ( t (BFGS.BFGSData n (BigFloat p)) )
remoteEvalGrads jbCfg points = do
  forConcurrently points (
    \point -> remoteEvalJob $
              cPtr (static evalSingleGrad) `cAp` closureDict `cAp`
              cPure jbCfg `cAp` cPure (1e-9) `cAp` cPure point
    )

-- | creates a database to cache checkpoints in BFGS search
-- (for use with Job monad)
newBoundingBoxCheckpointMap
 :: (KnownNat n)
 => Text
 -> V n Rational
 -> V n Rational
 -> Maybe FilePath
 -> Job (TrackedMap Job (V n Rational) FilePath)
newBoundingBoxCheckpointMap mapName bbMin bbMax mCheckpoint =
 liftIO (OPE.affineLocalityMap affine id mCheckpoint) >>=
 OPE.mkPersistent (DB.KeyValMap mapName)
 where
   affineShift = (bbMax + bbMin) / 2
   affineLinear = L.toRows $ L.diagonalMatrix $ (bbMax - bbMin) / 2
   affine = AffineTransform{..}
