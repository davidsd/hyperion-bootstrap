{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE OverloadedStrings #-}

module Hyperion.Bootstrap.SDPDeriv.Run where

import Control.Exception    (Exception, throw)
import Data.Aeson           (FromJSON, ToJSON, decodeStrict)
import Data.Attoparsec.Text qualified as AP
import Data.Binary          (Binary)
import Data.Maybe           (listToMaybe)
import Data.Text            (Text)
import Data.Text            qualified as Text
import Data.Text.Encoding   (encodeUtf8)
import GHC.Generics         (Generic)
import Hyperion.Log         qualified as Log
import System.Process       (readProcess)

data SDPDerivativeException = MkSDPDerivativeException
  { parserError :: String
  , parsedText  :: Text
  } deriving (Show, Exception)

data Mode = SDP2_B_b_c
  deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

data Input = MkInput
  { sdpDerivativeMode   :: Mode
  , sdp1Dir             :: FilePath
  , sdp1OutDir          :: FilePath
  , sdp2Dir             :: FilePath
  , precision           :: Int
  , approxObjExecutable :: FilePath
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

data Output a = MkOutput
  { linear    :: a
  , quadratic :: a
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON, Functor)

total :: Num a => Output a -> a
total o = linear o + quadratic o

inputToArgs :: Input -> [String]
inputToArgs i =
  [ "--sdp", sdp1Dir i ] ++
  [ "--newSdp", sdp2Dir i ] ++
  [ "--solutionDir", sdp1OutDir i ] ++
  [ "--precision", show (precision i) ]

-- see schema in docs/approx_objective_schema.json of SDPB repo
data ApproxObjectiveOutput = ApproxObjectiveOutput
  { path         :: Text
  , objective    :: Text
  , d_objective  :: Text
  , dd_objective :: Text
  } deriving (Eq, Ord, Show, Generic, Binary, ToJSON, FromJSON)

processAOOutput :: Fractional a => Text -> Either String (Output a)
processAOOutput contents = do
  -- throw out srun echo line, everything left is json
  jsonText <- AP.parseOnly (AP.skipWhile (/= '[') >> AP.takeText) contents
  -- make sure we have a valid list of ApproxObjectiveOutput objects
  let maybeAOOut = decodeStrict (encodeUtf8 jsonText) >>= listToMaybe
  aoOut <- case maybeAOOut of
    Just x  -> Right x
    Nothing -> Left "JSON parse error"
  -- turn the first one into an Output object
  lin <- AP.parseOnly AP.rational (d_objective aoOut)
  quad <- AP.parseOnly AP.rational (dd_objective aoOut)
  --let quad = 0
  return (MkOutput lin quad)

sdpDerivative :: (Fractional a) => Input -> IO (Output a)
sdpDerivative input = do
  let args = inputToArgs input
      command = approxObjExecutable input
  Log.info "Running sdp_derivative" (command, args)
  contents <- Text.pack <$> readProcess command args ""
  Log.info "approx_objective output:" contents
  pure $ case processAOOutput contents of
    Right result -> result
    Left err     -> throw (MkSDPDerivativeException err contents)

