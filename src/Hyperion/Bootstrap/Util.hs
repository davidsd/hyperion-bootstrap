{-# LANGUAGE OverloadedStrings #-}

module Hyperion.Bootstrap.Util
  ( ToPath(..)
  , mkTmpFilePath
  , dirScatteredHashBasePath
  , hashBasePath
  , mapChunksConcurrentlyMonitored
  , mapConcurrentlyMonitored
  , setShuffled
  , setShuffledChunks
  ) where

import Control.Concurrent      (forkIO, killThread, threadDelay)
import Control.Concurrent.MVar (modifyMVar_, newMVar, tryReadMVar)
import Control.Monad           (when)
import Control.Monad.IO.Class  (liftIO)
import Data.Binary             (Binary)
import Data.BinaryHash         (hashUntypedBase64Safe)
import Data.List               (sortOn)
import Data.List.Split         (chunksOf)
import Data.Set                (Set)
import Data.Set                qualified as Set
import Data.Text               (Text)
import Data.Time               (NominalDiffTime)
import Hyperion                (Job, mapConcurrently_)
import Hyperion.Log            qualified as Log
import Hyperion.Util           (nominalDiffTimeToMicroseconds, randomString)
import System.FilePath.Posix   ((</>))
import System.Random           (mkStdGen, randoms)
import Text.Printf             qualified as Printf

-- | A class for objects that can be assigned a FilePath, given a base
-- directory
class ToPath a where
  toPath
    :: FilePath -- ^ base directory
    -> a        -- ^ Object
    -> FilePath

hashBasePath :: Binary a => String -> a -> FilePath
hashBasePath tag a = tag <> "_" <> hashUntypedBase64Safe a

-- | Construct a path from a hash, with a directory made from the
-- first two characters of the hash to avoid putting too many files in
-- a single directory
dirScatteredHashBasePath :: Binary a => String -> a -> FilePath
dirScatteredHashBasePath tag obj = hashDir </> hashFile
  where
    (hashDir, hashFile) = case hashUntypedBase64Safe obj of
      s@(x : y : _) -> ("a"<>[x,y], tag <> "_" <> s)
      s             -> error $ "Expected a longer table hash: " <> s

mkTmpFilePath :: FilePath -> IO FilePath
mkTmpFilePath f = do
  let tmpstem = f <> ".tmp-"
  rand <- randomString 5
  return (tmpstem <> rand)

mapChunksConcurrentlyMonitored
  :: Text
  -> NominalDiffTime
  -> ([k] -> Job ())
  -> [[k]]
  -> Job ()
mapChunksConcurrentlyMonitored tag reportTime handleChunk chunks = do
  let numKeys = sum (map length chunks)
  when (numKeys > 0) $ do
    Log.info ("Computing " <> tag) numKeys
    todoVar <- liftIO $ newMVar numKeys
    let
      go chunk = do
        handleChunk chunk
        liftIO $ modifyMVar_ todoVar $ pure . (subtract (length chunk))
      monitor lastTodo = do
        threadDelay $ nominalDiffTimeToMicroseconds reportTime
        mNumTodo <- tryReadMVar todoVar
        case mNumTodo of
          Just n
            | n > 0 -> do
                when (n < lastTodo) $
                  Log.text $ tag <> " progress: " <> Log.showText (progressBar (numKeys - n) numKeys)
                monitor n
          _ -> pure ()
    threadId <- liftIO $ forkIO $ monitor $ numKeys + 1
    mapConcurrently_ go chunks
    liftIO $ killThread threadId

mapConcurrentlyMonitored
  :: Text
  -> NominalDiffTime
  -> (k -> Job ())
  -> [k]
  -> Job ()
mapConcurrentlyMonitored tag reportTime go ks =
  mapChunksConcurrentlyMonitored tag reportTime (mapM_ go) (map pure ks)

-- | Shuffle the list using a deterministic pseudo-random generator
shuffle :: [a] -> [a]
shuffle xs =
  map fst $ sortOn snd $ zip xs $ randoms @Int (mkStdGen 137)

setShuffled :: Set k -> [k]
setShuffled = shuffle . Set.toList

setShuffledChunks :: Int -> Set k -> [[k]]
setShuffledChunks chunkSize = chunksOf chunkSize . setShuffled

data ProgressBar = MkProgressBar Rational Rational

progressBar :: Real a => a -> a -> ProgressBar
progressBar x y = MkProgressBar (toRational x) (toRational y)

instance Show ProgressBar where
  show (MkProgressBar n total) =
    "[" ++ addArrow (replicate nEquals '=') ++ replicate nSpaces ' ' ++ "]" ++
    Printf.printf " %d/%d (%.1f%%)" (ceiling @_ @Int n) (ceiling @_ @Int total) percent
    where
      percent = 100 * fromRational @Double (n/total)
      nEquals = ceiling $ toRational len * n/total
      nSpaces = len - nEquals
      len = 40
      addArrow []     = []
      addArrow (_:cs) = reverse $ '>':cs
