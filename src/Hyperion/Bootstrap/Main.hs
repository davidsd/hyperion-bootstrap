{-# LANGUAGE ApplicativeDo       #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Hyperion.Bootstrap.Main where

import Control.Exception         (Exception)
import Control.Monad.Catch       (throwM, try)
import Control.Monad.Except      (ExceptT (..))
import Control.Monad.Trans.Maybe (exceptToMaybeT, runMaybeT)
import Data.Text                 (Text)
import Hyperion                  (Cluster, HyperionConfig, HyperionStaticConfig,
                                  hyperionMain)
import Hyperion qualified
import Hyperion.Database         qualified as DB
import Hyperion.Log              qualified as Log
import Options.Applicative

data BoundsProgramOptions = BoundsProgramOptions
  { programName     :: Text
  , initialDatabase :: Maybe FilePath
  } deriving Show

data UnknownProgram = UnknownProgram Text
  deriving (Show, Exception)

type BoundsProgram = Text -> Cluster ()

boundsProgramParser :: Parser BoundsProgramOptions
boundsProgramParser = do
  programName <-
    strOption (long "program"
               <> short 'p'
               <> metavar "NAME"
               <> help "Program to run")
  initialDatabase <- optional $
    strOption (long "initial-database"
               <> short 'd'
               <> metavar "PATH"
               <> help "Path to initial sqlite database (optional). \
                       \Will be copied and added to.")
  return BoundsProgramOptions{..}

hyperionBootstrapMain :: HyperionConfig -> HyperionStaticConfig -> BoundsProgram -> IO ()
hyperionBootstrapMain hyperionConfig hyperionStaticConfig boundsProgram =
  hyperionMain boundsProgramParser mkHyperionConfig hyperionStaticConfig runProgram
  where
    runProgram opts = do
      DB.insert (DB.KeyValMap "programName") (programName opts) ()
      boundsProgram (programName opts)
    mkHyperionConfig opts = hyperionConfig { Hyperion.initialDatabase = initialDatabase opts }

tryAllPrograms :: [BoundsProgram] -> BoundsProgram
tryAllPrograms programs pName =
  runMaybeT (asum (map tryProgram programs)) >>= \case
    Just result -> return result
    Nothing     -> Log.warn "Unknown program" pName
  where
    tryProgram p = exceptToMaybeT $ ExceptT $ try @Cluster @UnknownProgram $ p pName

unknownProgram :: Text -> Cluster ()
unknownProgram = throwM . UnknownProgram
